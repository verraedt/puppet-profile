# Go version of sensu
class profile::sensu::backend (
  String $admin_gui,
  String $mail_from,
  String $mail_to,
  String $mg_apikey,
  String $mg_domain,
) {
  include ::profile::sensu::agent
  include ::sensu::backend
  include ::sensu::plugins

  # Fix configure step in puppet module for clusters
  $password = lookup('sensu::password')
  exec { 'fix sensuctl configure step in clusters':
    command => "/usr/bin/sensuctl configure --trusted-ca-file /etc/sensu/ssl/ca.crt --non-interactive --url https://${::fqdn}:8080 --username admin --password ${password} || /bin/true",
    unless  => '/usr/bin/sensuctl cluster member-list',
    before  => Class['::sensu::backend'],
  }

  $mailgun = @(EOT)
    {
      "mailer-mailgun" :  {
        "admin_gui" : "<%= $admin_gui %>",
        "mail_from": "<%= $mail_from %>",
        "mail_to": "<%= $mail_to %>",
        "mg_apikey": "<%= $mg_apikey %>",
        "mg_domain": "<%= $mg_domain %>"
      }
    }
    | EOT

  file {
    default:
      owner => 'sensu',
      group => 'sensu',
      ;
    '/etc/sensu/conf.d':
      ensure => directory,
      ;
    '/etc/sensu/conf.d/mailgun.json':
      ensure  => file,
      mode    => '0600',
      content => inline_epp($mailgun),
      ;
  }
}
