class profile::role::desktop (
  Sensitive[String] $wifi_password,
  String $wifi_ssid
) {
  include ::profile::locale
  include ::profile::podman
  include ::profile::role::desktop::printer
  include ::profile::role::desktop::autofs
  include ::profile::role::desktop::packages

  profile::network { $wifi_ssid:
    type         => 'wifi',
    password     => $wifi_password,
    extra_params => {
      '802-11-wireless-security.key-mgmt' => 'wpa-psk',
    },
  }

  dconf::configuration {
    'org/gnome/desktop/wm/preferences':
      configuration => {
        '/'       => {
          'button-layout' => 'close,minimize,maximize:',
        },
      }
      ;
    'org/gnome/desktop/peripherals/touchpad':
      configuration => {
        '/'       => {
          'disable-while-typing'         => false,
          'two-finger-scrolling-enabled' => true,
        },
      }
      ;
  }

  sysctl { 'kernel.sysrq':
    ensure => present,
    value  => '1',
  }

  file_line { 'hide locadmin':
    path  => '/var/lib/AccountsService/users/locadmin',
    line  => 'SystemAccount=true',
    match => '^SystemAccount\=',
  }
}
