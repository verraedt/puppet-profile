class profile::role::cifsexport (
  Hash $env,
  String $keytab_lookup,
) {
  $directories = [
    '/srv/docker/samba',
    '/srv/docker/samba/export',
    '/srv/docker/samba/export/shared',
  ]

  file { $directories:
    ensure => directory,
  }

  ['shared'].each |$dir| {
    mount { "/srv/docker/samba/export/${dir}":
      ensure  => mounted,
      device  => "/data/${dir}",
      fstype  => none,
      options => 'default,bind',
    }
  }

  profile::container { 'samba':
    image      => 'registry.gitlab.com/verraedt/samba',
    docker_tag => 'master',
    volumes    => [
      '/srv/docker/samba/private:/var/lib/samba/private',
      '/srv/docker/samba/export:/data',
      '/var/lib/sss/pipes:/var/lib/sss/pipes',
    ],
    inject     => {
      '/etc/krb5.keytab' => {
        'lookup' => $keytab_lookup,
        'mode'   => '0600',
        'base64' => true,
      },
    },
    net        => 'host',
    env        => $env,
  }
}
