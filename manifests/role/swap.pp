class profile::role::swap (
  Integer $size = 2048,
) {
  exec { 'create swapfile':
    command => "/usr/bin/dd if=/dev/zero of=/swap count=${size} bs=1MiB",
    creates => '/swap',
    notify  => Exec['format swap'],
  }

  exec { 'format swap':
    command     => '/usr/sbin/mkswap /swap',
    refreshonly => true,
    notify      => Exec['swapon swap'],
  }

  file_line { 'swap in fstab':
    path    => '/etc/fstab',
    line    => '/swap none swap sw 0 0',
    match   => '^/swap ',
    require => Exec['format swap'],
    notify  => Exec['swapon swap'],
  }

  exec { 'swapon swap':
    command     => '/usr/sbin/swapon -a',
    refreshonly => true,
  }
}
