class profile::role::apcupsd (
  Hash[String,String] $config,
) {
  package { 'apcupsd':
    ensure => installed,
    notify => Service['apcupsd'],
  }

  $config.each |$key, $val| {
    file_line { "apcupsd setting ${key}":
      path   => '/etc/apcupsd/apcupsd.conf',
      line   => "${key} ${val}",
      match  => '^$key',
      notify => Service['apcupsd'],
    }
  }
  service { 'apcupsd':
    ensure => running,
    enable => true,
  }

  profile::container { 'apcupsd_exporter':
    image         => 'aaronsilber/apcupsd-exporter',
    docker_tag    => 'latest',
    exposed_ports => ['9000'],
    env           => {
      'SERVICE_NAME'    => $::hostname,
      'SERVICE_TAGS'    => 'prometheus',
      'APCUPSD_ADDR'    => "${::fqdn}:3551",
      'APCUPSD_NETWORK' => 'tcp',
      'TELEMETRY_ADDR'  => ':9000',
      'TELEMETRY_PATH'  => '/metrics',
    },
  }
}
