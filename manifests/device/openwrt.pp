define profile::device::openwrt (
  String $consul_host,
  String $digitalocean_token,
  String $domain,
  String $external_domain,
  String $unifi_controller,
  String $unifi_hostname,
  String $password,
  Array $forward_rules,
  Array $input_rules,
  Hash[String,String] $ipv4_routes,
  Hash[String,String] $ipv6_routes,
  Hash[String,String] $nrpe_ping_check,
  String $ipv4_global_network,
  String $ipv6_global_network,
  String $management_address,
  String $management_gateway,
  Array[Integer] $vlans = [],
  Hash[String,Hash[String,String]] $wifi_wpa2_enterprise = {},
) {
  $sanitized_name = regsubst($name, /([^a-zA-Z0-9-]+)/, '-', 'G')

  $config_files = {
    'dhcp' => 'dnsmasq',
    'dropbear' => 'dropbear',
    'firewall' => 'firewall',
    'network' => 'network',
    'system' => undef,
    'wireless' => 'network',
  }

  $services = ['dnsmasq', 'dropbear', 'firewall', 'network']

  $params = {
    'hostname' => $name,
    'domain' => $domain,
    'management_address' => $management_address,
    'management_gateway' => $management_gateway,
    'vlans' => $vlans,
    'wifi_wpa2_enterprise' => $wifi_wpa2_enterprise,
  }

  $tasks = $config_files.map |String $filename, Optional[String] $service| {
    {
      name   => $filename,
      openwrt_copy   => {
        content => epp("profile/openwrt/${filename}.epp", $params),
        dest => "/etc/config/${filename}",
      },
      notify => empty($service) ? { true => [], false => "restart ${service}" },
    }
  }

  $handlers = $services.map |String $service| {
    {
      name    => "restart ${service}",
      openwrt_service => {
        name   => $service,
        action => 'restart',
      }
    }
  }

  profile::ansible::playbook { $name:
    host        => ip_address($management_address),
    remote_user => 'root',
    password    => $password,
    tasks       => $tasks,
    handlers    => $handlers,
    execute     => false,
  }
}
