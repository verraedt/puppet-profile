class profile::role::server::raid_sleep (
  Array[String] $disks = $facts['disks_ata_wdc'],
) {
  if empty($disks) {
    $ensure = 'absent'
    $ensure_directory = 'absent'
    $ensure_service = 'stopped'
    $enable_service = false

    Service['raid-sleep'] -> File['/etc/raid-sleep.conf'] -> Package['raid-sleep']
  } else {
    $ensure = 'present'
    $ensure_directory = 'directory'
    $ensure_service = 'running'
    $enable_service = true

    Package['raid-sleep'] -> File['/etc/raid-sleep.conf'] ~> Service['raid-sleep']
  }

  package { 'raid-sleep':
    ensure => $ensure,
  }

  file {
    '/etc/raid-sleep.conf':
      ensure  => $ensure,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => epp('profile/raid-sleep.conf.epp', { 'disks' => $disks }),
      ;
    '/etc/raid-sleep.hourly':
      ensure => $ensure_directory,
      ;
    '/etc/raid-sleep.daily':
      ensure => $ensure_directory,
      ;
  }

  service { 'raid-sleep':
    ensure => $ensure_service,
    enable => $enable_service,
  }
}
