require 'tempfile'

Puppet::Functions.create_function(:'profile::inline_jinja') do
  dispatch :inline_jinja do
    param 'String', :template
    param 'Hash', :context
  end

  def inline_jinja(template, context)
    file = Tempfile.new('inlinejinja')
    file.write template
    file.close
    
    result = call_function('profile::jinja', file.path, context)
    file.unlink
    result.chomp
  end
end
