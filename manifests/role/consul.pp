class profile::role::consul (
  Array[String] $masters,
  Array[String] $recursors,
  String $secret,
  String $acl_agent_token = '',
  String $acl_master_token = '',
  Optional[String] $bind_ip = undef,
  String $ca_cert = lookup('ssl::consul::ca'),
  Optional[String] $cert = undef,
  Optional[String] $key = undef,
  String $node_name = $facts['fqdn'],
  Enum['server', 'client'] $type = 'client'
) {
  if ($facts['os']['family'] == 'RedHat' and versioncmp($facts['os']['release']['major'],'8') >= 0) {
    include ::profile::role::consul::disable_systemd_resolved

    Service['systemd-resolved'] -> Service['consul']
  }

  package { 'consul':
    ensure => installed,
  }

  $_cert = empty($cert) ? {
    true => lookup("ssl::consul::certs.'${node_name}'.certificate"),
    default => $cert,
  }
  $_key = empty($key) ? {
    true => lookup("ssl::consul::certs.'${node_name}'.private_key"),
    default => $key,
  }

  $params = {
    'type' => $type,
    'consul_masters' => $masters,
    'consul_secret' => $secret,
    'consul_acl_master_token' => $acl_master_token,
    'consul_acl_agent_token' => $acl_agent_token,
    'node_name' => $node_name,
    'recursors' => $recursors,
    'bind_ip' => $bind_ip,
  }

  file {
    default:
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package['consul'],
      notify  => Service['consul'],
      ;
    '/etc/sysconfig/consul':
      content => epp("profile/consul/${type}.sysconfig", $params),
      ;
    '/etc/consul.d':
      ensure => directory,
      mode   => '0755',
      ;
    '/etc/consul.d/ssl':
      ensure => directory,
      mode   => '0755',
      ;
    '/etc/consul.d/ssl/ca.crt':
      content => $ca_cert,
      ;
    "/etc/consul.d/ssl/${node_name}.crt":
      content => $_cert,
      ;
    "/etc/consul.d/ssl/${node_name}.key":
      content => $_key,
      mode    => '0600',
      ;
    '/var/local/consul':
      ensure => directory,
      ;
    '/etc/consul.d/encrypt.json':
      content => epp('profile/consul/encrypt.json.epp', $params),
      mode    => '0600',
      ;
    '/etc/consul.d/acl.json':
      content => epp('profile/consul/acl.json.epp', $params),
      mode    => '0600',
      ;
    '/etc/consul.d/performance.json':
      content => epp('profile/consul/performance.json.epp', $params),
      mode    => '0600',
      ;
    '/etc/consul.d/checks.json':
      content => epp('profile/consul/checks.json.epp', $params),
      mode    => '0600',
  }

  if $type == 'server' {
    $ensure = 'present'
  } else {
    $ensure = 'absent'
  }

  $systemd_override = @(EOT)
    [Service]
    ExecStop=
    | EOT

  systemd::dropin_file { 'consul.service':
    ensure   => $ensure,
    filename => 'override.conf',
    unit     => 'consul.service',
    content  => $systemd_override,
    notify   => Service['consul'],
  }

  service { 'consul':
    ensure => running,
    enable => true,
  }

  file_line { '/etc/resolv.conf':
    path  => '/etc/resolv.conf',
    line  => 'nameserver ::1',
    after => '^#',
  }
}
