define profile::role::tinc::host (
  String $external_address,
  String $public_key,
  String $private_key,
  String $vpn_name,
  String $local_hostname,
  String $hostname = $name,
  Optional[String] $port = undef,
  String $target_directory = '/etc/tinc',
) {
  if $hostname == $local_hostname {
    file { "${target_directory}/${vpn_name}/rsa_key.priv":
      owner   => 'root',
      group   => 'root',
      mode    => '0600',
      content => "\n${private_key}",
    }
  }

  if ($port) {
    $content = "Address = ${external_address}\nPort = ${port}\n\n${public_key}"
  } else {
    $content = "Address = ${external_address}\n\n${public_key}"
  }

  file { "${target_directory}/${vpn_name}/hosts/${hostname}":
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => $content,
  }
}
