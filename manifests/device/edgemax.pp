define profile::device::edgemax (
  String $ipv4_network,
  String $ipv6_network,
  String $ipv4_tinc_address,
  String $ipv6_tinc_address,
  String $ipv4_global_network,
  String $ipv6_global_network,
  String $router_id,
  String $domain,
  String $unifi_hostname,
  String $unifi_controller,
  String $password,
  String $consul_host,
  String $digitalocean_token,
  String $external_domain,
  Array[String] $tinc_connect_to,
  Hash[String,String] $nrpe_ping_check,
  Array $forward_rules = [],
  Array $input_rules = [],
  Boolean $enable_pppoe = false,
  Boolean $enable_poe = false,
  String $pppoe_username = '',
  String $pppoe_password = '',
  Integer $pppoe_vlan = 0,
  Array $pppoe_nameservers = [],
  Hash[String,String] $static_arp = {},
  Hash[String,String] $static_leases = {},
  Optional[String] $static_public_ipv6_address = undef,
  Hash[String,String] $ipv4_routes = {},
  Hash[String,String] $ipv6_routes = {},
  Hash[Integer,Hash[String,String]] $additional_vlans = {},
  String $tftp_ip = '',
  String $tinc_name = $name,
  String $tinc_vpn_name = lookup('profile::role::tinc::vpn_name'),
  Hash[String,Hash[String,String]] $tinc_hosts = lookup('profile::role::tinc::hosts'),
  Hash[Variant[Integer,String],String] $port_forwards = {},
  Integer $vlan_offset = 0,
  String $ipv4_dns_name = $name,
) {
  $sanitized_name = regsubst($name, /([^a-zA-Z0-9-]+)/, '-', 'G')
  $directory = "/tmp/device-edgemax-${sanitized_name}"

  file { [$directory, "${directory}/post-config.d", "${directory}/user-data", "${directory}/user-data/tinc"]:
    ensure => directory,
    group  => 'root',
    owner  => 'root',
    mode   => '0750',
  }

  $zone_names = {
    'internal' => 'INTERNAL',
    'guest' => 'GST',
    'lan' => 'LAN',
    'management' => 'MGT',
    'iot' => 'IOT',
    'public' => 'WAN',
  }

  $public_interfaces = $enable_pppoe ? {
    true    => ['pppoe0', profile::vlan('switch0', 90 + $vlan_offset)],
    default => [profile::vlan('switch0', 90 + $vlan_offset)],
  }

  $zone_interfaces = {
    'internal' => ['br0', profile::vlan('switch0', 10 + $vlan_offset), profile::vlan('switch0', 15 + $vlan_offset)] + $additional_vlans.filter |$vlan, $info| { $info[zone] == 'internal' }.map |$vlan, $info| { "switch0.${vlan}" },
    'guest' => [profile::vlan('switch0', 50 + $vlan_offset)] + $additional_vlans.filter |$vlan, $info| { $info[zone] == 'guest' }.map |$vlan, $info| { "switch0.${vlan}" },
    'lan' => [profile::vlan('switch0', 20 + $vlan_offset), profile::vlan('switch0', 40 + $vlan_offset), profile::vlan('switch0', 41 + $vlan_offset)] + $additional_vlans.filter |$vlan, $info| { $info[zone] == 'lan' }.map |$vlan, $info| { "switch0.${vlan}" },
    'management' => [profile::vlan('switch0', 2 + $vlan_offset)] + $additional_vlans.filter |$vlan, $info| { $info[zone] == 'management' }.map |$vlan, $info| { "switch0.${vlan}" },
    'iot' => [profile::vlan('switch0', 30 + $vlan_offset)] + $additional_vlans.filter |$vlan, $info| { $info[zone] == 'iot' }.map |$vlan, $info| { "switch0.${vlan}" },
    'public' => $public_interfaces,
  }

  $forward_rules_processed = $forward_rules.map |$rule| {
    if empty($rule[services]) {
      $rule
    } else {
      $rule[services].flatten.map |$service| {
        $parts = $service.split('/')
        if length($parts) > 1 {
          merge($rule, { 'protocol' => $parts[0], 'port' => $parts[1] })
        } else {
          merge($rule, { 'protocol' => $service })
        }
      }
    }
  }.flatten

  $input_rules_processed = $input_rules.map |$rule| {
    $rule[services].flatten.map |$service| {
      $parts = $service.split('/')
      if length($parts) > 1 {
        merge($rule, { 'protocol' => $parts[0], 'port' => $parts[1] })
      } else {
        merge($rule, { 'protocol' => $service })
      }
    }
  }.flatten

  $_params = {
    'enable_pppoe' => $enable_pppoe,
    'enable_poe' => $enable_poe,
    'static_arp' => $static_arp,
    'static_leases' => $static_leases,
    'ipv4_routes' => $ipv4_routes.filter |$k, $v| { $k != $ipv4_network },
    'ipv6_routes' => $ipv6_routes.filter |$k, $v| { $k != $ipv6_network },
    'ipv4_tinc_address' => $ipv4_tinc_address,
    'ipv6_tinc_address' => $ipv6_tinc_address,
    'ipv4_network' => $ipv4_network,
    'ipv6_network' => $ipv6_network,
    'ipv4_global_network' => $ipv4_global_network,
    'ipv6_global_network' => $ipv6_global_network,
    'router_id' => $router_id,
    'additional_vlans' => $additional_vlans,
    'vlan2_ipv4_address' => ip_increment(ip_subnet($ipv4_network, 24, 2), 1),
    'vlan10_ipv4_address' => ip_increment(ip_subnet($ipv4_network, 24, 10), 1),
    'vlan15_ipv4_address' => ip_increment(ip_subnet($ipv4_network, 24, 15), 1),
    'vlan20_ipv4_address' => ip_increment(ip_subnet($ipv4_network, 24, 20), 1),
    'vlan30_ipv4_address' => ip_increment(ip_subnet($ipv4_network, 24, 30), 1),
    'vlan40_ipv4_address' => ip_increment(ip_subnet($ipv4_network, 24, 40), 1),
    'vlan41_ipv4_address' => ip_increment(ip_subnet($ipv4_network, 24, 41), 1),
    'vlan50_ipv4_address' => ip_increment(ip_subnet($ipv4_network, 24, 50), 1),
    'vlan2_ipv6_address' => ip_increment(ip_subnet($ipv6_network, 64, 2), 1),
    'vlan10_ipv6_address' => ip_increment(ip_subnet($ipv6_network, 64, 10), 1),
    'vlan15_ipv6_address' => ip_increment(ip_subnet($ipv6_network, 64, 15), 1),
    'vlan20_ipv6_address' => ip_increment(ip_subnet($ipv6_network, 64, 20), 1),
    'vlan30_ipv6_address' => ip_increment(ip_subnet($ipv6_network, 64, 30), 1),
    'vlan40_ipv6_address' => ip_increment(ip_subnet($ipv6_network, 64, 40), 1),
    'vlan50_ipv6_address' => ip_increment(ip_subnet($ipv6_network, 64, 50), 1),
    'static_public_ipv6_address' => $static_public_ipv6_address,
    'tftp_ip' => $tftp_ip,
    'domain' => $domain,
    'hostname' => $name,
    'unifi_hostname' => $unifi_hostname,
    'unifi_controller' => $unifi_controller,
    'zone_names' => $zone_names,
    'zone_interfaces' => $zone_interfaces,
    'forward_rules' => $forward_rules_processed,
    'input_rules' => $input_rules_processed,
    'domains' => lookup('profile::role::dnsmasq::domains'),
    'addresses' => lookup('profile::role::dnsmasq::addresses'),
    'dns_servers' => lookup('profile::role::dnsmasq::dns_servers'),
    'nrpe_ping_check' => $nrpe_ping_check,
    'pppoe_username' => $pppoe_username,
    'pppoe_password' => $pppoe_password,
    'pppoe_vlan' => $pppoe_vlan,
    'pppoe_nameservers' => $pppoe_nameservers,
    'port_forwards' => $port_forwards,
    'repo_password' => lookup('repo_password'),
    'vlan_offset' => $vlan_offset,
  }

  $variants = ['minimal', 'wireguard', 'firewall']

  $variants.each |String $variant| {
    $config = "${directory}/config.${variant}"

    $params = $_params + { 'variant' => $variant }

    concat { $config:
      ensure => present,
      notify => Profile::Ansible::Playbook[$name],
    }

    concat::fragment { "${config} firewall":
      target  => $config,
      content => epp('profile/edgemax/firewall.epp', $params),
      order   => '10',
    }

    concat::fragment { "${config} interfaces":
      target  => $config,
      content => epp('profile/edgemax/interfaces.epp', $params),
      order   => '20',
    }

    concat::fragment { "${config} policy":
      target  => $config,
      content => epp('profile/edgemax/policy.epp', $params),
      order   => '21',
    }

    concat::fragment { "${config} port forward":
      target  => $config,
      content => epp('profile/edgemax/port-forward.epp', $params),
      order   => '25',
    }

    concat::fragment { "${config} protocols":
      target  => $config,
      content => epp('profile/edgemax/protocols.epp', $params),
      order   => '30',
    }

    concat::fragment { "${config} service":
      target  => $config,
      content => epp('profile/edgemax/service.epp', $params),
      order   => '40',
    }

    concat::fragment { "${config} system":
      target  => $config,
      content => epp('profile/edgemax/system.epp', $params),
      order   => '50',
    }

    concat::fragment { "${config} zone-policy":
      target  => $config,
      content => epp('profile/edgemax/zone-policy.epp', $params),
      order   => '60',
    }

    concat::fragment { "${config} footer":
      target  => $config,
      content => epp('profile/edgemax/footer.epp'),
      order   => '99',
    }
  }

  $params = $_params

  file { "${directory}/user-data/dnsmasq.conf":
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0600',
    content => epp('profile/edgemax/dnsmasq.conf.epp', $params),
    notify  => Profile::Ansible::Playbook[$name],
  }

  $post_config_hooks = [
    'symlinks.sh',
    'dynamic_firewall.sh',
    'drop_empty_udp.sh',
    'fix_etc_hosts.sh',
  ]

  $config_scripts = [
    'setup',
    'cleanup-config',
    'check_ipsets',
    'check_iptables',
    'dhcp.sh',
  ] + $post_config_hooks.map |$name| { "post-config.d/${name}" }

  $config_scripts.each |$script| {
    $content = $script ? {
      'post-config.d/dynamic_firewall.sh' => epp("profile/edgemax/${script}.epp", $params),
      'check_iptables' => epp("profile/edgemax/${script}.epp", $params),
      default => file("profile/edgemax/${script}"),
    }

    file { "${directory}/${script}":
      ensure  => file,
      owner   => root,
      group   => root,
      mode    => '0600',
      content => $content,
      notify  => Profile::Ansible::Playbook[$name],
    }
  }

  profile::role::tinc::network { "${directory} network ${tinc_vpn_name}":
    vpn_name         => $tinc_vpn_name,
    local_hostname   => $tinc_name,
    connect_to       => $tinc_connect_to,
    hosts            => $tinc_hosts,
    target_directory => "${directory}/user-data/tinc",
    notify           => Profile::Ansible::Playbook[$name],
  }

  file { "${directory}/user-data/tinc/nets.boot":
    content => "${tinc_vpn_name} --debug=2\n",
    notify  => Profile::Ansible::Playbook[$name],
  }

  file { "${directory}/nrpe.cfg":
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0600',
    content => epp('profile/edgemax/nrpe.cfg.epp', $params),
    notify  => Profile::Ansible::Playbook[$name],
  }

  $dev_90 = profile::vlan('switch0', 90 + $vlan_offset)

  $df_options = @(EOT)
    OPTIONS="--consul http://<%= $consul_host %>:8500"
    | EOT

  $du_options = @(EOT)
    OPTIONS="--hostname <%= $name %> --hostname-ipv4 <%= $ipv4_dns_name %> --interface <% if $enable_pppoe { %>pppoe0<% } else { %><%= $dev_90 %><% } %> --do-token <%= $digitalocean_token %> --do-domain <%= $external_domain %><% if $static_public_ipv6_address { %> --address <%= $static_public_ipv6_address %><% } %>"
    | EOT

  profile::ansible::playbook { $name:
    host        => $router_id,
    remote_user => 'ubnt',
    password    => $password,
    execute     => false,
    tasks       => [
      {
        name       => 'copy configuration to router',
        copy       => {
          src  => "${directory}/config.{{ item }}",
          dest => '/home/ubnt/config.{{ item }}',
        },
        with_items => $variants,
      },
      {
        name   => 'dnsmasq configuration',
        copy   => {
          src  => "${directory}/user-data/dnsmasq.conf",
          dest => '/config/user-data/dnsmasq.conf',
        },
        notify => 'restart dnsmasq',
      },
      {
        name   => 'dnsmasq configuration symlink',
        file   => {
          src   => '/config/user-data/dnsmasq.conf',
          dest  => '/etc/dnsmasq.d/local.conf',
          state => 'link',
        },
        notify => 'restart dnsmasq',
      },
      {
        name       => 'config hooks and scripts',
        copy       => {
          src   => "${directory}/{{ item }}",
          dest  => '/config/scripts/{{ item }}',
          owner => 'root',
          group => 'root',
          mode  => '0700',
        },
        with_items => $config_scripts,
      },
      {
        name       => 'post commit hooks',
        file       => {
          src   => '/config/scripts/post-config.d/{{ item }}',
          dest  => '/etc/commit/post-hooks.d/{{ item }}',
          state => 'link',
        },
        with_items => $post_config_hooks,
      },
      #{
      #  name       => 'dhcp hook',
      #  lineinfile => {
      #    path   => '/opt/vyatta/sbin/on-dhcp-event.sh',
      #    line   => 'DHCPfile="/config/scripts/dhcp.sh"; if [ -x "$DHCPfile" ]; then sh "$DHCPfile" "$action" "$client_name" "$client_ip" "$client_mac" "$domain"; fi; exit 0',
      #    regexp => 'exit 0$',
      #  },
      #},
      {
        name => 'tinc folder',
        file => {
          dest  => '/config/user-data/tinc',
          state => 'directory',
        },
      },
      {
        name => 'tinc symlink',
        file => {
          dest  => '/etc/tinc',
          src   => '/config/user-data/tinc',
          state => 'link',
        },
      },
      {
        name => 'apt update',
        raw  => 'apt-get update',
      },
      {
        name       => 'packages',
        raw        => 'apt-get install -y --allow-unauthenticated {{ item }}',
        with_items => ['dynamic-firewall', 'update-dns', 'tinc', 'bridge-utils', 'nagios-nrpe-server', 'nagios-plugins-basic', 'wireguard', 'mosquitto-clients'],
      },
      {
        name   => 'tinc configuration',
        copy   => {
          src  => "${directory}/user-data/tinc",
          dest => '/config/user-data/',
          mode => 'preserve',
        },
        notify => 'restart tinc',
      },
      {
        name => 'fix tinc cronjob',
        copy => {
          dest    => '/etc/cron.d/fix-tinc',
          owner   => 'root',
          group   => 'root',
          mode    => '0644',
          content => "53 * * * * root /bin/sh -c 'brctl show br0 | grep -q verraedt || brctl addif br0 verraedt'",
        },
      },
      {
        name   => 'dynamic-firewall options',
        copy   => {
          dest    => '/etc/default/dynamic-firewall',
          owner   => 'root',
          group   => 'root',
          mode    => '0644',
          content => inline_epp($df_options),
        },
        notify => 'restart dynamic-firewall',
      },
      {
        name   => 'update-dns options',
        copy   => {
          dest    => '/etc/default/update-dns',
          owner   => 'root',
          group   => 'root',
          mode    => '0644',
          content => inline_epp($du_options),
        },
        notify => 'restart update-dns',
      },
      {
        name   => 'nrpe options',
        copy   => {
          dest    => '/etc/default/nagios-nrpe-server',
          owner   => 'root',
          group   => 'root',
          mode    => '0644',
          content => "PIDDIR=/var/run/nrpe\n",
        },
        notify => ['kill nrpe', 'restart nrpe'],
      },
      {
        name   => 'nrpe configuration',
        copy   => {
          src   => "${directory}/nrpe.cfg",
          dest  => '/etc/nagios/nrpe.cfg',
          owner => 'root',
          group => 'root',
          mode  => '0644',
        },
        notify => 'restart nrpe',
      },
      {
        name       => 'enable services',
        service    => {
          name    => '{{ item }}',
          state   => 'started',
          enabled => 'yes',
        },
        with_items => [
          'tinc@verraedt',
          'dynamic-firewall',
          'update-dns',
          'nagios-nrpe-server',
        ],
      },
      {
        name => 'clean apt',
        raw  => 'apt-get clean',
      }
    ],
    handlers    => [
      {
        name    => 'restart dnsmasq',
        service => {
          name  => 'dnsmasq',
          state => 'restarted',
        }
      },
      {
        name    => 'restart dynamic-firewall',
        service => {
          name  => 'dynamic-firewall',
          state => 'restarted',
        }
      },
      {
        name    => 'restart update-dns',
        service => {
          name  => 'update-dns',
          state => 'restarted',
        }
      },
      {
        name    => 'restart tinc',
        service => {
          name  => 'tinc@verraedt',
          state => 'restarted',
        }
      },
      {
        name => 'kill nrpe',
        raw  => 'ps aux | grep nrpe | grep -v grep | awk \'{print $2}\' | xargs kill',
      },
      {
        name    => 'restart nrpe',
        service => {
          name  => 'nagios-nrpe-server',
          state => 'restarted',
        }
      },
    ],
  }
}
