define profile::container::inject (
  String $path,
  String $container,
  Optional[String] $lookup = undef,
  Optional[String] $source = undef,
  Optional[String] $jinja = undef,
  Optional[Hash] $yaml = undef,
  Optional[Hash] $json = undef,
  Optional[String] $content = undef,
  Hash $context = {},
  String $owner   = 'root',
  String $group   = 'root',
  String $mode    = '0644',
  Boolean $base64 = false,
) {
  $id = regsubst($path, '/', '-', 'G')

  if ($lookup) {
    $_content = lookup($lookup)
    if ($base64) {
      $real_content = base64('decode', $_content)
    } else {
      $real_content = $_content
    }
  } elsif ($jinja) {
    $real_content = profile::jinja($jinja, $context)
  } elsif ($yaml) {
    $real_content = to_yaml($yaml)
  } elsif ($json) {
    $real_content = to_json($json)
  } else {
    $real_content = $content
  }

  file { "/srv/container/${container}/${id}":
    content => $real_content,
    source  => $source,
    owner   => $owner,
    group   => $group,
    mode    => $mode,
    seltype => 'container_file_t',
    require => File["/srv/container/${container}"],
    notify  => Docker::Run[$container],
  }
}
