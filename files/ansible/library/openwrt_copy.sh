#!/bin/sh
# Copyright (c) 2017 Markus Weippert
# GNU General Public License v3.0 (see https://www.gnu.org/licenses/gpl-3.0.txt)

changed=false
msg="Did nothing"

source $1

tmpfile=$(mktemp)
echo "$content" > $tmpfile

cmp -s $tmpfile $dest || {
  cp $tmpfile $dest
  changed=true
  msg="Updated content"
}

rm -f $tmpfile

printf '{"changed": %s, "msg": "%s"}' "$changed" "$msg"
