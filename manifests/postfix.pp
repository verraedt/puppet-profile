class profile::postfix (
  String $domain,
  String $ldap_search_base,
  String $ldap_server
) {
  $mailname = "${::hostname}.${domain}"

  package { ['postfix', 'postfix-ldap']:
    ensure => installed,
    notify => Service['postfix'],
  }

  file {
    default:
      ensure  => file,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package['postfix'],
      before  => Service['postfix'],
      ;
    '/etc/mailname':
      content => "${mailname}\n",
      notify  => Service['postfix'],
      ;
    '/etc/aliases':
      content => epp('profile/postfix/aliases.epp', { 'domain' => $domain }),
      notify  => Exec['newaliases'],
      ;
    '/etc/postfix':
      ensure => directory,
      group  => 'postfix',
      mode   => '0755',
      ;
    '/etc/postfix/main.cf':
      content => epp('profile/postfix/main.epp', { 'mailname' => $mailname }),
      notify  => Service['postfix'],
      ;
    '/etc/postfix/master.cf':
      content => epp('profile/postfix/master.epp'),
      notify  => Service['postfix'],
      ;
    '/etc/postfix/ldap-aliases-user-forward.cf':
      content => epp('profile/postfix/ldap-aliases-user-forward.epp', { 'ldap_server' => $ldap_server, 'ldap_search_base' => $ldap_search_base }),
      notify  => Service['postfix'],
      ;
    '/var/spool/postfix/etc':
      ensure  => directory,
      mode    => '0755',
      seltype => postfix_spool_t,
      ;
    '/var/spool/postfix/etc/services':
      source  => '/etc/services',
      force   => false,
      seltype => postfix_spool_t,
      ;
  }

  exec { 'newaliases':
    command     => '/usr/bin/newaliases',
    refreshonly => true,
    require     => Package['postfix'],
  }

  service { 'postfix':
    ensure => running,
    enable => true,
  }
}
