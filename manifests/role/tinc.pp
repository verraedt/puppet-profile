class profile::role::tinc (
  String $ipv4_address,
  String $ipv6_address,
  String $vpn_name,
  Boolean $adjust_firewall = false,
  Array[String] $bind_to = ['* 655'],
  Array[String] $connect_to = [],
  String $dns1 = '172.16.226.2',
  String $domain = 'net.verraedt.be',
  String $hostname = $facts['hostname'],
  Hash[String,Hash[String,String]] $hosts = {},
  Hash[String,String] $ipv4_routes = {},
  Hash[String,String] $ipv6_routes = {},
  Boolean $networkmanager_auto_connect = false, # Only supported if use_systemd_wildcard is set to true
  Boolean $use_systemd_wildcard = true
) {
  include ::profile::role::ospf

  package { ['tinc', 'bridge-utils']:
    ensure => installed,
    notify => Service['tinc'],
  }

  file { '/etc/tinc':
    ensure  => directory,
    require => Package['tinc'],
  }

  if $networkmanager_auto_connect {
    $_ipv4_address = sprintf('%s/32', ip_address($ipv4_address))
    $_ipv6_address = sprintf('%s/128', ip_address($ipv6_address))
    $toggle_ipv4_address = $ipv4_address
    $toggle_ipv6_address = $ipv6_address
  } else {
    $_ipv4_address = $ipv4_address
    $_ipv6_address = $ipv6_address
    $toggle_ipv4_address = undef
    $toggle_ipv6_address = undef
  }

  network::interface { 'br0':
    type     => 'Bridge',
    ipaddr   => ip_address($_ipv4_address),
    netmask  => ip_netmask($_ipv4_address),
    ipv6init => 'yes',
    ipv6addr => $_ipv6_address,
    mtu      => 1280,
    onboot   => 'yes',
    zone     => 'internal',
    domain   => $domain,
    dns1     => $dns1,
  }

  $ips = $ipv4_routes.map |$index,$value| { ip_address($index) } + $ipv6_routes.map |$index,$value| { ip_address($index) }
  $masks = $ipv4_routes.map |$index,$value| { ip_netmask($index) } + $ipv6_routes.map |$index,$value| { ip_netmask($index) }
  $gws = $ipv4_routes.map |$index,$value| { $value } + $ipv6_routes.map |$index,$value| { $value }
  $families = $ipv4_routes.map |$index,$value| { 'inet4' } + $ipv6_routes.map |$index,$value| { 'inet6' }

  network::route { 'br0':
    ipaddress => $ips,
    netmask   => $masks,
    gateway   => $gws,
    family    => $families,
  }

  $systemd_override = @(EOT)
    [Unit]
    After=shorewall.service
    After=shorewall6.service
    | EOT

  systemd::dropin_file { 'tinc.service':
    filename => 'override.conf',
    unit     => 'tinc.service',
    content  => $systemd_override,
  }

  $systemd_wildcard_override = @(EOT)
    [Service]
    ExecStart=
    ExecStart=/usr/sbin/tincd --debug=2 -n %i -D
    | EOT

  systemd::dropin_file { 'tinc@.service':
    filename => 'override.conf',
    unit     => 'tinc@.service',
    content  => $systemd_wildcard_override,
  }

  profile::role::tinc::network { $vpn_name:
    vpn_name            => $vpn_name,
    local_hostname      => $hostname,
    bind_to             => $bind_to,
    connect_to          => $connect_to,
    hosts               => $hosts,
    toggle_ipv4_address => $toggle_ipv4_address,
    toggle_ipv6_address => $toggle_ipv6_address,
    require             => Package['tinc'],
    notify              => Service['tinc'],
  }

  if $use_systemd_wildcard {
    if $networkmanager_auto_connect {
      file { '/etc/NetworkManager/dispatcher.d/tinc.sh':
        ensure  => file,
        content => file('profile/tinc/tinc.sh'),
        owner   => root,
        group   => root,
        mode    => '0750',
        require => Service['tinc'],
      }
    } else {
      file { '/etc/NetworkManager/dispatcher.d/tinc.sh':
        ensure => absent,
      }

      service { "tinc@${vpn_name}":
        enable => true,
        notify => Service['tinc'],
      }
    }
  } else {
    file_line { $vpn_name:
      ensure => present,
      path   => '/etc/tinc/nets.boot',
      line   => "${vpn_name} --debug=2",
      notify => Service['tinc'],
    }
  }

  service { 'tinc':
    ensure => running,
    enable => true,
  }

  if $adjust_firewall {
    firewalld_custom_service { 'ospf':
      ensure      => present,
      short       => 'ospf',
      description => 'OSPF route exchange protocol',
      protocols   => ['ospf'],
      ports       => [],
    }

    ['ospf', 'ssh'].each |String $service| {
      firewalld_service { "Allow ${service} from the internal zone":
        ensure  => present,
        service => $service,
        zone    => 'internal',
      }
    }
  }

  file { '/etc/NetworkManager/dispatcher.d/kuleuven.sh':
    ensure => absent,
  }
}
