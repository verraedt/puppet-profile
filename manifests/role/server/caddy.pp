class profile::role::server::caddy (
  String $consul_token,
  String $jwt_secret,
  String $digitalocean_token = lookup('profile::dns::digitalocean_token'),
  Hash[String,String] $env = {},
  Optional[String] $ipv4 = undef,
  Optional[String] $ipv6 = undef,
  Hash[String,Hash] $inject = {},
  Hash[String,Hash] $sites = {},
  Hash $labels = {},
  ) {
  include ::profile::role::server::www_user
  include ::profile::role::glusterfs

  if length($sites) == 0 {
    $ensure_file = absent
    $ensure_present = absent
    $ensure_running = absent
  } else {
    $ensure_file = file
    $ensure_present = present
    $ensure_running = running
  }

  $_env = {
    'CADDYPATH' => '/etc/ssl/caddy',
    'DO_AUTH_TOKEN' => $digitalocean_token,
    'JWT_SECRET' => $jwt_secret,
    'CONSUL_TOKEN' => $consul_token,
    'SERVICE_443_NAME' => "caddy-${facts['hostname']}",
    'SERVICE_TAGS' => 'fw.internal,fw.external',
    'SERVICE_IPV4' => 'no',
    'SERVICE_80' => 'no',
  } + $env

  $domains = $sites.map |$k,$v| {
    if $v['domains'] {
      $v['domains']
    } else {
      $k
    }
  }.flatten.map |$d| {
    $d.regsubst(/\/.*$/, '', 'G')
  }.sort.unique

  $rules = $domains.map |$domain| { "HostSNI(`${domain}`)" }

  $_labels = {
    'traefik.enable' => 'true',
    'traefik.on' => $facts['hostname'],
    "traefik.tcp.routers.caddy-${facts['hostname']}.rule" => join($rules, ' || '),
    "traefik.tcp.routers.caddy-${facts['hostname']}.tls.passthrough" => 'true',
    "traefik.tcp.routers.caddy-${facts['hostname']}.entrypoints" => 'https',
  } + $labels

  profile::container { 'caddy':
    ensure  => $ensure_running,
    image   => 'registry.gitlab.com/verraedt/caddy',
    volumes => [
      'caddy-ssl:/etc/ssl/caddy',
      '/etc/ssl/sensu-ca.crt:/etc/ssl/sensu-ca.crt:ro',
      '/etc/consul.d/ssl/ca.crt:/etc/consul.d/ssl/ca.crt:ro',
      '/etc/caddy:/etc/caddy:ro',
      '/srv/www:/srv/www',
      '/var/log/caddy:/var/log/caddy',
    ],
    env     => $_env,
    inject  => $inject,
    labels  => $_labels,
    require => File['/etc/ssl/sensu-ca.crt'],
  }

  $mail = @(EOT)
    Hello,

    The following message was submitted to the contact form on your website:

    Sender: {{.Form.Get "name"}}
    Address: {{.Form.Get "email"}}

    {{.Form.Get "message"}}
    | EOT 

  file {
    default:
      ensure  => $ensure_file,
      owner   => 'root',
      group   => 'www-data',
      mode    => '0640',
      notify  => Profile::Container['caddy'],
      require => [
        User['www-data'],
        Group['www-data'],
      ],
      ;
    '/etc/caddy':
      ensure => directory,
      mode   => '0755',
      ;
    '/etc/ssl/caddy':
      ensure => directory,
      mode   => '0770',
      ;
    '/etc/caddy/mail.txt':
      content => inline_epp($mail),
      ;
    '/etc/caddy/id_rsa':
      owner => 'www-data',
      mode  => '0600',
      ;
    '/var/log/caddy':
      ensure => directory,
      mode   => '0755',
      ;
  }

  concat { '/etc/caddy/Caddyfile':
    ensure  => $ensure_present,
    require => File['/etc/caddy'],
    notify  => Profile::Container['caddy'],
  }

  create_resources('profile::role::server::caddy::site', $sites)
}
