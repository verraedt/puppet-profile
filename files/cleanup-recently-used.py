#!/usr/bin/env python
import xml.etree.ElementTree as xee
import argparse
import datetime
import re

now = datetime.datetime.utcnow()
delta = datetime.timedelta(minutes=25)

def is_recent(timestamp):
    timestamp = re.sub('\..*Z', 'Z', timestamp)
    return datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%SZ') + delta >= now

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="History file to cleanup", type=argparse.FileType('r'))
    args = parser.parse_args()
    
    doc = xee.parse(args.file.name)
    root = doc.getroot()

    changed = False
    for tag in root.findall('bookmark'):
        if not tag.attrib['href'].startswith('file:///nfs'):
            continue

        recent = is_recent(tag.attrib['added']) or is_recent(tag.attrib['modified']) or is_recent(tag.attrib['visited'])
        
        for subtag in tag.iter('{http://www.freedesktop.org/standards/desktop-bookmarks}application'):
            recent = recent or is_recent(subtag.attrib['modified'])

        if not recent:
            root.remove(tag)
            changed = True
    if changed:
        doc.write(args.file.name)
