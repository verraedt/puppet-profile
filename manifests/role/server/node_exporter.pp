class profile::role::server::node_exporter {
  package { 'node-exporter':
    ensure => latest, #lint:ignore:package_ensure
    notify => Service['node-exporter'],
  }

  $options = '--collector.filesystem.ignored-mount-points ^/(sys|proc|dev|host|etc|rootfs/var/lib/docker/containers|rootfs/var/lib/docker/overlay2|rootfs/run/docker/netns|rootfs/var/lib/docker/aufs)($$|/)'

  file { '/etc/sysconfig/node-exporter':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => "OPTIONS=\"${options}\"\n",
    require => Package['node-exporter'],
    notify  => Service['node-exporter'],
  }

  service { 'node-exporter':
    ensure => running,
    enable => true,
  }
}
