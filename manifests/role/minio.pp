class profile::role::minio (
  String $access_key,
  String $node_name,
  String $secret_key,
  String $bind_ip = lookup('profile::role::consul::bind_ip'),
  String $ca_cert = lookup('ssl::consul::ca'),
  Optional[String] $cert = undef,
  Optional[String] $fqdn_cert = undef,
  Optional[String] $fqdn_key = undef,
  Optional[String] $key = undef,
  String $servers = 'minio{1...2}.net.verraedt.be',
  String $volume = '/usr/local/share/minio'
) {
  $_cert = empty($cert) ? {
    true => lookup("ssl::consul::certs.'${node_name}'.certificate"),
    default => $cert,
  }
  $_key = empty($key) ? {
    true => lookup("ssl::consul::certs.'${node_name}'.private_key"),
    default => $key,
  }

  $_fqdn_cert = empty($fqdn_cert) ? {
    true => lookup("ssl::consul::certs.'${facts['fqdn']}'.certificate"),
    default => $fqdn_cert,
  }
  $_fqdn_key = empty($fqdn_key) ? {
    true => lookup("ssl::consul::certs.'${facts['fqdn']}'.private_key"),
    default => $fqdn_key,
  }

  package { 'minio':
    ensure   => installed,
    provider => 'rpm',
    source   => 'https://dl.min.io/server/minio/release/linux-amd64/minio-20220730052140.0.0.x86_64.rpm',
  }

  $config = @(EOT)
    MINIO_ROOT_USER="<%= $access_key %>"
    MINIO_VOLUMES="https://<%= $servers %>:9000<%= $volume %>/data{1...2}"
    MINIO_OPTS="--address <%= $bind_ip %>:9000 --certs-dir /etc/minio/certs --console-address :9001"
    MINIO_ROOT_PASSWORD="<%= $secret_key %>"
    MINIO_SERVER_URL="https://<%= $node_name %>:9000"
    | EOT

  file { '/etc/default/minio':
    ensure  => file,
    content => inline_epp($config),
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    require => Package['minio'],
    notify  => Service['minio'],
  }

  group { 'minio-user':
    system  => true,
  }

  user { 'minio-user':
    system  => true,
    gid     => 'minio-user',
    home    => $volume,
    shell   => '/usr/sbin/nologin',
    require => Group['minio-user'],
  }

  file {
    default:
      ensure  => directory,
      owner   => 'minio-user',
      group   => 'minio-user',
      mode    => '0700',
      require => [
        Package['minio'],
        User['minio-user'],
        Group['minio-user'],
      ],
      before  => Service['minio'],
      ;
    '/etc/minio':
      ;
    '/etc/minio/certs':
      ;
    '/etc/minio/certs/CAs':
      ;
    '/etc/minio/certs/CAs/ca.crt':
      ensure  => file,
      mode    => '0600',
      content => $ca_cert,
      ;
    '/etc/minio/certs/private.key':
      ensure  => file,
      mode    => '0600',
      content => $_key,
      ;
    '/etc/minio/certs/public.crt':
      ensure  => file,
      mode    => '0600',
      content => $_cert,
      ;
    "/etc/minio/certs/${facts['fqdn']}":
      ;
    "/etc/minio/certs/${facts['fqdn']}/private.key":
      ensure  => file,
      mode    => '0600',
      content => $_fqdn_key,
      ;
    "/etc/minio/certs/${facts['fqdn']}/public.crt":
      ensure  => file,
      mode    => '0600',
      content => $_fqdn_cert,
      ;
    $volume:
      ;
    "${volume}/data1":
      mode => '0755',
      ;
    "${volume}/data2":
      mode => '0755',
      ;
  }

  service { 'minio':
    ensure => running,
    enable => true,
  }
}
