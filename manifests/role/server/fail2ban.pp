class profile::role::server::fail2ban {
  class { 'fail2ban':
    bantime  => '86400',
    ignoreip => '127.0.0.0/8 172.16.0.0/16',
  }

  fail2ban::jail {
    'ssh-force-iptables':
      port         => '1:65535',
      filter       => 'sshd-force',
      backend      => 'systemd',
      journalmatch => 'sshd',
      action       => 'firewallcmd-new',
      maxretry     => 30,
      bantime      => 3600,
      findtime     => 120,
      notify       => Service['fail2ban'],
      ;
    'ssh-iptables':
      port         => '1:65535',
      filter       => 'sshd',
      backend      => 'systemd',
      journalmatch => 'sshd',
      action       => 'firewallcmd-new',
      maxretry     => 10,
      findtime     => 600,
      notify       => Service['fail2ban'],
      ;
  }

  fail2ban::filter {
    'sshd-force':
      failregexes      => [
        '^%(__prefix_line)sAccepted publickey for .* from <HOST> port .* ssh2: .*$',
      ],
      additional_defs  => ['_daemon = sshd'],
      additional_inits => ['journalmatch = _SYSTEMD_UNIT=sshd.service + _COMM=sshd'],
      additional_incs  => ['before = common.conf', 'after = '],
      notify           => Service['fail2ban'],
  }
}
