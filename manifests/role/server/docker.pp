class profile::role::server::docker (
  String $ipv4_range,
  String $ipv6_range,
  Optional[String] $vpn_ipv4_range = undef,
  Optional[String] $vpn_ipv6_range = undef,
  Hash[String,Hash] $container_config = {},
  Array[String] $containers = []
) {
  include ::profile::role::ospf
  include ::profile::role::dynamic_firewall
  include ::profile::role::consul
  include ::profile::role::server::registrator
  include ::profile::role::server::firewalld
  include ::profile::role::server::www_user

  include ::profile::users

  if $facts['os']['name'] == 'AlmaLinux' {
    $distro = 'centos'
  } else {
    $distro = downcase($facts['os']['name'])
  }

  file { '/etc/yum.repos.d/docker-ce.repo':
    source  => "https://download.docker.com/linux/${distro}/docker-ce.repo",
    replace => false,
  }

  $packages = [
    'docker-ce',
    # 'docker-compose',
    # 'docker-python',
    'device-mapper-persistent-data',
    'lvm2',
  ]

  package { $packages:
    require => [
      File['/etc/yum.repos.d/docker-ce.repo'],
      Group['docker'],
    ],
    notify  => Service['docker'],
  }

  $ipv6_range_addr = ip_address($ipv6_range);
  $systemd_override = @(EOT)
    [Service]
    EnvironmentFile=-/etc/sysconfig/docker
    ExecStartPre=-/bin/sh -c '/sbin/ip a del dev docker0 <%= $ipv6_range_addr %>/80'
    ExecStart=
    ExecStart=/usr/bin/dockerd $OPTIONS
    [Unit]
    After=shorewall.service
    After=shorewall6.service
    | EOT

  systemd::dropin_file { 'docker.service':
    filename => 'override.conf',
    unit     => 'docker.service',
    content  => inline_epp($systemd_override),
    before   => Service['docker'],
  }

  if has_key($facts['networking']['interfaces'], 'br0') {
    $dns = $facts['networking']['interfaces']['br0']['ip']
    $dns6 = $facts['networking']['interfaces']['br0']['ip6']
  } else {
    $dns = $facts['networking']['ip']
    $dns6 = $facts['networking']['ip6']
  }
  $ip = ip_address(ip_increment($ipv4_range, 1));
  $ip6 = ip_address(ip_increment($ipv6_range, 1));

  file { '/etc/sysconfig/docker':
    ensure => file,
  }
  -> file_line { '/etc/sysconfig/docker':
    path  => '/etc/sysconfig/docker',
    line  => "OPTIONS='--iptables=false --dns ${dns6} --dns ${dns} --ip-masq=false --bip=${ip}/26 --fixed-cidr=${ip}/26 --ipv6 --fixed-cidr-v6 ${ip6}/80 --experimental --metrics-addr 0.0.0.0:9301'",
    match => '^OPTIONS=',
  }

  $daemon_config = @(EOT)
    {
      "default-address-pools": [
        { "base": "<%= $ipv4_range %>", "size": 28 },
        { "base": "<%= $ipv6_range %>", "size": 80 }
      ]
    }
    | EOT


  file { '/etc/docker/daemon.json':
    ensure  => file,
    content => inline_epp($daemon_config),
    before  => Service['docker'],
    require => Package['docker-ce'],
  }

  service { 'docker':
    ensure => running,
    enable => true,
  }

  sysctl { ['net.ipv4.ip_forward', 'net.ipv6.conf.all.forwarding']:
    ensure => present,
    value  => '1',
    before => Service['docker'],
  }

  sysctl { 'net.ipv6.conf.all.accept_ra':
    ensure => present,
    value  => '2',
    before => Service['docker'],
  }

  selinux::fcontext { '/srv/docker(/.*)?':
    seltype => 'container_file_t',
    seluser => 'sysadm_u',
  }

  selinux::fcontext { '/srv/container(/.*)?':
    seltype => 'container_file_t',
    seluser => 'sysadm_u',
    before  => File['/srv/container'],
  }

  file { '/srv/container':
    ensure => directory,
  }

  firewalld_direct_rule { 'Masquerade outgoing ipv6 traffic from docker':
    ensure        => present,
    inet_protocol => 'ipv6',
    table         => 'nat',
    chain         => 'POSTROUTING',
    priority      => 0,
    args          => "-d 2000::/3 -s ${ipv6_range} -j MASQUERADE",
  }

  if $vpn_ipv4_range and $vpn_ipv6_range {
    docker_network { 'Docker VPN network':
      name             => 'vpn',
      driver           => 'bridge',
      subnet           => [
        $vpn_ipv4_range,
        $vpn_ipv6_range,
      ],
      options => [
        'com.docker.network.bridge.name=vpn0',
      ],
      additional_flags => ['--ipv6'],
    }
  }

  $servers = join(lookup('profile::role::glusterfs::peers'), ',')

  docker::plugin { 'mikebarkmin/glusterfs':
    plugin_alias => 'glusterfs',
    settings     => ["SERVERS=${servers}", 'VOLNAME=vol1'],
  }

  $containers.each |$container| {
    profile::container { $container:
      * => $container_config[$container],
    }
  }

  (keys($container_config) - $containers).each |$container| {
    profile::container { $container:
      ensure => absent,
      image  => $container_config[$container]['image'],
    }
  }
}
