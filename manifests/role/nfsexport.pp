class profile::role::nfsexport (
  Hash $env,
  String $keytab_lookup,
) {
  service { ['rpcbind.socket', 'rpcbind']:
    ensure => stopped,
    enable => false,
  }

  $directories = [
    '/srv/docker/nfs',
    '/srv/docker/nfs/export',
    '/srv/docker/nfs/export/private',
    '/srv/docker/nfs/export/shared',
    '/srv/docker/nfs/export/scratch',
  ]

  file { $directories:
    ensure => directory,
  }

  ['private', 'shared', 'scratch'].each |$dir| {
    mount { "/srv/docker/nfs/export/${dir}":
      ensure  => mounted,
      device  => "/data/${dir}",
      fstype  => none,
      options => 'default,bind',
    }
  }

  mount { '/srv/docker/nfs/export/srv':
    ensure  => absent,
  }

  kmod::load { ['nfs', 'nfsd', 'rpcsec_gss_krb5']: }
  -> profile::container { 'nfs':
    image            => 'registry.gitlab.com/verraedt/nfs',
    docker_tag       => 'master',
    volumes          => [
      '/srv/docker/nfs/export:/data',
      '/var/lib/sss/pipes:/var/lib/sss/pipes',
    ],
    inject           => {
      '/etc/krb5.keytab' => {
        'lookup' => $keytab_lookup,
        'mode'   => '0600',
        'base64' => true,
      },
    },
    extra_parameters => ['--cap-add=SYS_ADMIN'],
    net              => 'host',
    env              => $env,
  }
}
