#!/bin/bash

action=$1
mac=$2
ip=$3
name=$4

vlan=$(expr $(echo 172.16.104.235 | cut -d. -f3) % 64)
domain=lan${vlan}.$(grep 'ns\.lan2\.' /etc/dnsmasq.d/local.conf | cut -d. -f3 | head -n1).net.verraedt.be

sanitized_mac="$(echo "${mac}" | tr -d ':')"

function _pub() {
    DEST=$(host -t AAAA mosquitto-1883.service.consul 172.16.226.2 | awk '{print $5}')
    mosquitto_pub -h $DEST -r "$@"
}

# Don't report on deleted leases
case "$action" in
    "add"|"old")
        location="home"

        #/opt/vyatta/sbin/on-dhcp-event.sh commit "$name" "$ip" "$mac" "$domain" enable disable
        ;;
    "del")
        location="not_home"

        #/opt/vyatta/sbin/on-dhcp-event.sh release "$name" "$ip" "$mac" "$domain" enable disable
        ;;
    *)
        location="unknown"
        ;;
esac

_pub -t "location/${sanitized_mac}" -m "${location}"
