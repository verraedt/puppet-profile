Puppet::Functions.create_function(:'profile::vlan') do
  dispatch :vlan do
    param 'String', :device
    param 'Integer', :vlan
  end

  def vlan(device, vlan)
    "#{device}.#{vlan}"
  end
end
