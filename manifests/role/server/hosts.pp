class profile::role::server::hosts (
  Hash[String,String] $ipv4,
  Hash[String,String] $ipv6,
) {
  $ipv4_addresses = unique($ipv4.map |String $k, String $v| {
    $v
  })

  $ipv4_addresses.each |String $ip| {
    $aliases = $ipv4.filter |String $k, String $v| {
      $v == $ip
    }.map |String $k, String $v| {
      $k
    }
    
    $first_alias = $aliases[0]
    $first_alias_short = regsubst($first_alias, '.net.verraedt.be$', '')

    host { $first_alias:
      ensure       => present,
      ip           => $ip,
      host_aliases => $aliases - [$first_alias] + [$first_alias_short],
    }
  }

  $ipv6_addresses = unique($ipv6.map |String $k, String $v| {
    $v
  })

  $ipv6_addresses.each |String $ip| {
    $aliases = $ipv6.filter |String $k, String $v| {
      $v == $ip
    }.map |String $k, String $v| {
      $k
    }
    
    $first_alias = $aliases[0]
    $first_alias_short = regsubst($first_alias, '.net.verraedt.be$', '')

    host { "${first_alias_short}-6":
      ensure       => present,
      ip           => $ip,
      host_aliases => $aliases + [$first_alias_short],
    }
  }
}
