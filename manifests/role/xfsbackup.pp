class profile::role::xfsbackup {
  include ::profile::role::server::backup

  $directories = ['/backups', '/backups/cold', '/backups/warm', '/backups/cold/xfs', '/backups/warm/xfs']

  file { $directories:
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  cron {
    default:
      user => 'root',
      ;
    'backup xfs weekly':
      command => '/usr/sbin/backup-xfs --target /backups/cold/xfs --level 0 --keep 2 / /var /srv /home',
      minute  => '30',
      hour    => '19',
      weekday => '6',
      ;
    'backup xfs daily':
      command => '/usr/sbin/backup-xfs --target /backups/warm/xfs --level 1 --keep 3 / /var /srv /home',
      minute  => '30',
      hour    => '19',
      weekday => '0-5',
      ;
    'backup xfs hourly':
      command => '/usr/sbin/backup-xfs --target /backups/warm/xfs --level 2 --keep 4 / /var /srv /home',
      minute  => '30',
      hour    => ['0-18', '20-23'],
      ;
  }
}
