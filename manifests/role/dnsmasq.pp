class profile::role::dnsmasq (
  Hash[String,Array[String]] $addresses = {},
  Array[String] $dns_servers = [],
  Array[String] $domains = []
) inherits profile::params {
  package { 'dnsmasq':
    ensure => installed,
    notify => Service['dnsmasq'],
  }

  $params = {
    'interfaces' => $profile::params::interfaces,
    'addresses' => $addresses,
    'domains' => $domains,
    'dns_servers' => $dns_servers,
    'fqdn' => $facts['fqdn'],
  }

  file {
    default:
      require => Package['dnsmasq'],
      notify  => Service['dnsmasq'],
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      ;
    '/etc/dnsmasq.conf':
      content => epp('profile/dnsmasq.conf.epp', $params),
      ;
    '/etc/dnsmasq.hosts':
      ;
  }

  service { 'dnsmasq':
    ensure => running,
    enable => true,
  }

  file_line { '/etc/resolv.conf':
    path  => '/etc/resolv.conf',
    line  => 'nameserver ::1',
    after => '.*',
  }
}
