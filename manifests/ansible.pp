class profile::ansible {
  package { 'ansible':
    ensure => installed,
  }
  -> anchor { 'ansible': }

  file { '/root/.ssh/id_rsa_ansible':
    source => '/root/secrets/certs/ssh/ansible.key',
    mode   => '0600',
    before => Anchor['ansible'],
  }

  file { '/etc/ansible/ansible.cfg':
    content => file('profile/ansible.cfg'),
    require => Package['ansible'],
    before  => Anchor['ansible'],
  }

  concat { '/etc/ansible/hosts':
    ensure => present,
    mode   => '0600',
    before => Anchor['ansible'],
  }

  file { '/usr/share/ansible/plugins/modules/':
    ensure  => directory,
    recurse => 'remote',
    source  => 'puppet:///modules/profile/ansible/library',
    before  => Anchor['ansible'],
  }
}
