#!/bin/bash
if ! /sbin/iptables -t raw -L PREROUTING | grep -q 239.255.42.42 ; then
  /sbin/iptables -t raw -I PREROUTING -p udp -m length --length 28 -d 239.255.42.42 -j DROP
fi
