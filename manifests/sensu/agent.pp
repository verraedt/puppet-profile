# Go version of sensu
class profile::sensu::agent {
  include ::sensu::repo
  include ::sensu::agent
  include ::sensu::plugins

  file { '/opt/sensu':
    ensure => directory,
    owner  => 'sensu',
    group  => 'sensu',
  }

  file { '/var/cache/sensu':
    ensure => directory,
    owner  => 'sensu',
    group  => 'sensu',
  }

  file { '/etc/ssl/sensu-ca.crt':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => lookup('ssl::sensu::ca'),
  }
}
