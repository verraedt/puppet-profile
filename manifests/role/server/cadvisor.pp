class profile::role::server::cadvisor {
  profile::container { 'cadvisor_exporter':
    image         => 'google/cadvisor',
    command       => '-docker_only -housekeeping_interval 5s -disable_metrics tcp,udp,sched,disk',
    exposed_ports => ['8080'],
    volumes       => [
      '/:/rootfs:ro',
      '/var/run:/var/run:ro',
      '/sys:/sys:ro',
      '/var/lib/docker:/var/lib/docker:ro',
      '/dev/disk:/dev/disk:ro',
    ],
    env           => {
      'SERVICE_NAME' => "${::hostname}-cadvisor-exporter",
      'SERVICE_TAGS' => 'prometheus',
    },
  }
}
