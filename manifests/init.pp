class profile {
  include ::profile::puppetapply
  include ::profile::secrets
  include ::profile::keyboard
  include ::profile::time
  include ::profile::profile
  include ::profile::packages
  include ::profile::dns
  include ::profile::motd
  include ::profile::ldap
  include ::profile::kerberos
  include ::profile::sssd
  include ::profile::users
  include ::profile::ssh
  include ::profile::repositories
  include ::profile::postfix
  include ::profile::cron

  class { 'selinux':
    mode              => 'enforcing',
    type              => 'targeted',
    module_build_root => '/var/lib/puppet-deployment/puppet-selinux',
  }

  service { 'bareos-fd':
    ensure => stopped,
    enable => false,
  }

  service { 'bareos-dir':
    ensure => stopped,
    enable => false,
  }

  service { 'bareos-sd':
    ensure => stopped,
    enable => false,
  }
}
