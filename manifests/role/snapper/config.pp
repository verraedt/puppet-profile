define profile::role::snapper::config (
  String $mount,
  String $config = $name,
  Boolean $mirror = false,
  Enum['default', 'xfs'] $template = 'default',
) {
  $yes_no = $mirror ? {
    true => 'yes',
    default => 'no',
  }

  exec { "create-config ${config}":
    command => "/usr/bin/snapper -c ${config} create-config -t ${template} ${mount}",
    creates => "/etc/snapper/configs/${config}",
    require => Anchor['snapper-files'],
  }
  -> file_line { "/etc/snapper/configs/${config}":
    path  => "/etc/snapper/configs/${config}",
    line  => "SNAPSHOT_MIRROR=\"${yes_no}\"",
    match => '^SNAPSHOT_MIRROR=',
  }
}
