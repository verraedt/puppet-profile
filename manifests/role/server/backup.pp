class profile::role::server::backup (
  Hash[String,Hash] $authorized_keys = {},
  String $rsync_release = '6.el7.1+patch219334388',
  String $rsync_version = '3.1.2'
) {
  package { 'backup-scripts':
    ensure => latest, #lint:ignore:package_ensure
  }

  package { 'rsync':
    ensure => "${rsync_version}-${rsync_release}",
  }

  if $facts['os']['family'] == 'RedHat' and Integer($facts['os']['release']['major']) > 7 {
    yum::versionlock { 'rsync':
      ensure  => present,
      version => $rsync_version,
      release => $rsync_release,
      arch    => 'x86_64',
      epoch   => 0,
      require => Package['rsync'],
    }
  } else {
    yum::versionlock { "0:rsync-${rsync_version}-${rsync_release}.x86_64":
      ensure  => present,
      require => Package['rsync'],
    }
  }

  cron { 'sync backups':
    command => '/usr/sbin/sync-backups',
    user    => 'root',
    hour    => '19',
    minute  => '30',
    weekday => '6',
  }

  group { 'backup':
    system  => true,
  }

  file { '/data':
    ensure => directory,
  }

  user { 'backup':
    system     => true,
    gid        => 'backup',
    home       => '/data/backup',
    shell      => '/bin/bash',
    comment    => 'Backup user',
    managehome => true,
    require    => Group['backup'],
  }

  file { '/usr/local/sbin/rrsync':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => [
      "/usr/share/doc/rsync-${rsync_version}/support/rrsync",
      '/usr/share/doc/rsync/support/rrsync',
    ],
  }

  file { '/data/backup/.ssh':
    ensure  => directory,
    owner   => 'backup',
    group   => 'backup',
    mode    => '0700',
    seltype => 'ssh_home_t',
    require => User['backup'],
  }

  $authorized_keys.each |$label, $val| {
    ssh_authorized_key { "backup authorized key ${label}":
      user    => 'backup',
      require => User['backup'],
      notify  => Exec['fix backup context'],
      options => [
        "command=\"/usr/local/bin/rrsync /data/backup/${label}\"",
        'no-agent-forwarding',
        'no-port-forwarding',
        'no-pty',
        'no-user-rc',
        'no-X11-forwarding',
      ],
      *       => $val,
    }
  }

  exec { 'fix backup context':
    command => '/usr/bin/chcon -t ssh_home_t /data/backup/.ssh/authorized_keys',
    unless  => '/bin/ls -Z /data/backup/.ssh/authorized_keys | /usr/bin/grep -q ssh_home_t',
  }
}
