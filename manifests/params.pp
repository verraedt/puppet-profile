class profile::params (
  Boolean $docker_trusted = true,
  String $ipv4_network = '172.16.0.0/16',
  String $ipv6_network = 'fd00:76b8:b991::/48'
) {
  $interfaces = $facts['networking']['interfaces'].map |String $device, Hash $info| {
    case $device {
      'br0': {
        $hash = { 'device' => $device, 'ip' => lookup('profile::role::tinc::ipv4_address'), 'ip6' => lookup('profile::role::tinc::ipv6_address'), 'internal' => true, 'external' => false }
      }
      default: {
        # Select a good IPv4 address
        if !empty($info['ip']) {
          $good_ip4s = $info['bindings'].filter |$binding| {
            ip_supernet("${binding['address']}/32", ip_prefixlength($ipv4_network)) == $ipv4_network and $binding['netmask'] != '255.255.255.255'
          }

          if !empty($good_ip4s) {
            $good4 = $good_ip4s[-1]
            $mask4 = ip_prefixlength("${good4['address']}/${good4['netmask']}")
            $ip4 = "${good4['address']}/${mask4}"
          } else {
            $ip4 = ''
          }
        } else {
          $ip4 = ''
        }

        # Select a good IPv6 address
        if !empty($info['ip6']) {
          $good_ip6s = $info['bindings6'].filter |$binding| {
            ip_supernet("${binding['address']}/128", ip_prefixlength($ipv6_network)) == $ipv6_network and $binding['netmask'] != 'ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff' and $binding['address'][-1,1] != ':'
          }

          if !empty($good_ip6s) {
            $good6 = $good_ip6s[-1]
            $mask6 = ip_prefixlength("${good6['address']}/${good6['netmask']}")
            $ip6 = "${good6['address']}/${mask6}"
          } else {
            $ip6 = ''
          }

          $public_ip6s = $info['bindings6'].filter |$binding| {
            ip_supernet("${binding['address']}/128", 3) == '2000::/3' and $binding['address'][-1,1] != ':'
          }

          if !empty($public_ip6s) {
            $public6 = $public_ip6s[-1]
            $public_mask6 = ip_prefixlength("${public6['address']}/${public6['netmask']}")
            $public_ip6 = "${public6['address']}/${public_mask6}"
          } else {
            $public_ip6 = ''
          }
        } else {
          $ip6 = ''
          $public_ip6 = ''
        }

        $internal = !empty($ip4) or !empty($ip6)
        $external = !$internal and !empty($info['ip']) and $device != 'lo'
        $hash = { 'device' => $device, 'ip' => $ip4, 'ip6' => $ip6, 'public_ip6' => $public_ip6, 'internal' => $internal, 'external' => $external }
      }
    }
    $hash
  }

  notify { "interfaces: ${interfaces}": }

  $public_interfaces = $interfaces.filter |$k, $v| { $v[external] }.map |$k, $v| { $v[device] }
  if $docker_trusted {
    $internal_interfaces = $interfaces.filter |$k, $v| { $v[internal] and $v[device] !~ /^docker/ and $v[device] !~ /^br-/ }.map |$k, $v| { $v[device] }
    $trusted_interfaces = $interfaces.filter |$k, $v| { $v[internal] and ($v[device] =~ /^docker/ or $v[device] =~ /^br-/) }.map |$k, $v| { $v[device] }
  } else {
    $internal_interfaces = $interfaces.filter |$k, $v| { $v[internal] }.map |$k, $v| { $v[device] }
    $trusted_interfaces = []
  }

  $internal_interfaces_not_bridge = $interfaces.filter |$k, $v| { $v[internal] and $v[device] !~ /^docker/ and $v[device] !~ /^br-/ and $v[device] !~ /^vpn/ }.map |$k, $v| { $v[device] }

  $internal_public_ip6 = $interfaces.filter |$k, $v| { $v[internal] and $v[public_ip6] != '' }.map |$k,$v| { $v[public_ip6] }

  $internal_ip4 = $interfaces.filter |$k, $v| { $v[internal] and $v[device] !~ /^docker/ and $v[device] !~ /^br-/ and $v[device] !~ /^vpn/ and $v[ip] != '' }.map |$k,$v| { $v[ip] }
  $internal_ip6 = $interfaces.filter |$k, $v| { $v[internal] and $v[device] !~ /^docker/ and $v[device] !~ /^br-/ and $v[device] !~ /^vpn/ and $v[ip6] != '' }.map |$k,$v| { $v[ip6] }
}
