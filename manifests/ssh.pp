class profile::ssh (
  Hash[String,Hash] $authorized_keys = {},
  Hash[String,Array[String]] $authorized_principals = {},
  Array[String] $known_host_ca_keys = [],
  String $root_ssh_key = lookup("ssh_keys.root@${::hostname}.private_key"),
  String $ssh_config = '',
  Array[String] $trusted_user_ca_keys = []
) {
  include ::ssh

  $ca_keys = join($known_host_ca_keys, "\n")
  $trusted_users = join($trusted_user_ca_keys, "\n")

  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      ;
    '/etc/sudoers.d/ssh':
      mode    => '0440',
      content => "Defaults  env_keep+=SSH_AUTH_SOCK\n",
      ;
    '/root/.ssh':
      ensure => directory,
      mode   => '0700',
      ;
    '/root/.ssh/id_rsa':
      mode    => '0600',
      content => $root_ssh_key,
      ;
    '/etc/ssh/ssh_known_host_ca_keys':
      mode    => '0644',
      content => "${ca_keys}\n",
      ;
    '/etc/ssh/trusted_user_ca_keys':
      mode    => '0644',
      content => "${trusted_users}\n",
      ;
    '/etc/ssh/ssh_config_custom':
      mode    => '0644',
      content => $ssh_config,
      ;
    '/etc/ssh/auth_principals':
      ensure => directory,
      mode   => '0700',
      ;
  }

  $authorized_keys.each |$label, $val| {
    ssh_authorized_key { "root authorized key ${label}":
      user => 'root',
      *    => $val,
    }
  }

  $authorized_principals.each |$user, $principals| {
    $_principals = join($principals, "\n")

    file { "/etc/ssh/auth_principals/${user}":
      ensure  => file,
      mode    => '0600',
      content => "${_principals}\n",
    }
  }
}
