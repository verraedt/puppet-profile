class profile::role::server::portainer (
  String $agent_secret,
  Optional[String] $cert = undef,
  Optional[String] $key = undef,
  Boolean $server = false,
  String $image_agent = 'portainer/agent',
  String $image_server = 'portainer/portainer-ee',
  String $virtual_host = 'portainer.verraedt.be',
) {
  if ($server) {
    $node_name = "${::hostname}-portainer.service.consul"

    $_cert = empty($cert) ? {
      true => lookup("ssl::consul::certs.'${node_name}'.certificate"),
      default => $cert,
    }
    $_key = empty($key) ? {
      true => lookup("ssl::consul::certs.'${node_name}'.private_key"),
      default => $key,
    }
 
    file {
      default:
        ensure  => file,
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        ;
      '/etc/portainer':
        ensure => directory,
        mode   => '0755',
        ;
      '/etc/portainer/ssl':
        ensure => directory,
        mode   => '0755',
        ;
      "/etc/portainer/ssl/${node_name}.crt":
        content => $_cert,
        ;
      "/etc/portainer/ssl/${node_name}.key":
        content => $_key,
        mode    => '0600',
        ;
    }

    profile::container { 'portainer':
      image         => $image_server,
      exposed_ports => ['9443'],
      volumes       => [
        '/var/run/docker.sock:/var/run/docker.sock',
        '/var/lib/docker/volumes:/var/lib/docker/volumes',
        '/srv/portainer:/data',
        '/etc/portainer/ssl:/ssl:ro',
      ],
      env           => {
        'SERVICE_9443_NAME' => "${::hostname}-portainer",
        'SERVICE_8000' => 'no',
        'SERVICE_9000' => 'no',
        'AGENT_SECRET' => $agent_secret,
        'VIRTUAL_HOST' => $virtual_host,
      },
      privileged    => true,
      command       => "--sslcert /ssl/${node_name}.crt --sslkey /ssl/${node_name}.key",
      labels        => {
        'traefik.enable' => 'true',
        'traefik.on' => $facts['hostname'],
        'traefik.http.routers.portainer.rule' => "Host(`${virtual_host}`)",
        'traefik.http.routers.portainer.service' => "${::hostname}-portainer",
        "traefik.http.services.${::hostname}-portainer.loadbalancer.server.scheme" => 'https',
        "traefik.http.services.${::hostname}-portainer.loadbalancer.serverstransport" => "${::hostname}-portainer@file",
      },
    }
  } else {
    profile::container { 'portainer':
      image         => $image_agent,
      exposed_ports => ['9001'],
      volumes       => [
        '/var/run/docker.sock:/var/run/docker.sock',
        '/var/lib/docker/volumes:/var/lib/docker/volumes',
      ],
      env           => {
        'SERVICE_NAME' => "${::hostname}-portainer",
        'AGENT_SECRET' => $agent_secret,
      },
      privileged    => true,
    }
  }
}
