class profile::secrets (
  Optional[String] $key_path = undef,
  Optional[String] $secret_repo = undef
) {
  tag 'early'

  if $facts['os']['family'] == 'RedHat' and Integer($facts['os']['release']['major']) == 7 {
    exec { '/usr/bin/yum -y localinstall https://rpms.southbridge.ru/rhel7/stable/x86_64/git-crypt-0.6.1-1.el7.x86_64.rpm':
      before => Package['git-crypt'],
      unless => '/usr/bin/rpm -q git-crypt',
    }
  }

  package { 'git-crypt':
    ensure => installed,
  }

  if $secret_repo {
    file { '/root/secrets':
      ensure  => directory,
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      require => Vcsrepo['/root/secrets'],
    }

    vcsrepo { '/root/secrets':
      ensure   => latest,
      user     => 'root',
      provider => git,
      depth    => 1,
      source   => $secret_repo,
    }

    if $key_path {
      Package['git-crypt'] -> Vcsrepo['/root/secrets']

      exec { 'unlock /root/secrets':
        command => "/usr/bin/git-crypt unlock ${key_path}",
        cwd     => '/root/secrets',
        require => [
          Package['git-crypt'],
          Vcsrepo['/root/secrets'],
        ],
        unless  => '/usr/bin/test -f /root/secrets/.git/git-crypt/keys/default',
      }
    }
  }
}
