require 'open3'
require 'json'

Puppet::Functions.create_function(:'profile::jinja') do
  dispatch :jinja do
    param 'String', :file
    param 'Hash', :context
  end

  def jinja(file, context)
    path = call_function('find_template', [file])

    Open3.popen3('/usr/bin/jinja_template') do |i, o, t|
      i.puts path
      i.puts context.to_json
      i.close

      err = t.read
      raise ArgumentError, err unless err.empty?
      o.read
    end
  end
end
