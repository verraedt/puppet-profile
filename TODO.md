```puppet
  dconf::gnome_shell_extension {
    [
      'desktop-icons@csoriano',
      'places-menu@gnome-shell-extensions.gcampax.github.com' ,
      'apps-menu@gnome-shell-extensions.gcampax.github.com',
      'suspend-button@laserb'
    ]:
      ;
    'custom-hot-corners@janrunx.gmail.com':
      configuration => {
        'actions' => [
          "=(0, true, true, 'toggleOverview', '')",
          "=(0, false, false, 'toggleOverview', '')",
        ]
      }
      ;
  }
  dconf::gnome_shell_favorite_app {
    $::profile::extra_gnome_shell_favorite_apps:
      ;
    [
      'firefox.desktop',
      'org.gnome.Nautilus.desktop',
      'org.gnome.Software.desktop',
      'org.gnome.Terminal.desktop',
    ]:
      ;
  }
```
