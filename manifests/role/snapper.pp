class profile::role::snapper (
  Hash[String,Hash] $configs = {},
  Integer $cron_hour = 19,
  Integer $cron_minute = 40,
  Enum['auto', 'cron', 'raid-sleep'] $cron_provider = 'auto',
  Integer $cron_stage_hour = 4,
  Integer $cron_stage_minute = 0,
  Optional[String] $remote = undef,
  Optional[String] $remote_mac = undef
) {
  package { ['snapper', 'snapper-mirror']:
    ensure => installed,
    before => Anchor['snapper-files'],
  }

  if $cron_provider == 'auto' {
    $_cron_provider = empty($facts['disks_ata_wdc']) ? {
      true => 'cron',
      default => 'raid-sleep',
    }
  } else {
    $_cron_provider = $cron_provider
  }

  $other_provider = $_cron_provider ? {
    'cron' => 'raid-sleep',
    'raid-sleep' => 'cron',
  }

  $params = {
    'sync' => !empty($remote),
    'remote' => $remote,
    'remote_mac' => $remote_mac,
  }

  file {
    default:
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      require => Package['snapper'],
      before  => Anchor['snapper-files'],
      ;
    "/etc/${_cron_provider}.hourly/snapper":
      ensure  => file,
      content => epp('profile/snapper/cron.hourly.epp', $params),
      ;
    "/etc/${_cron_provider}.daily/snapper":
      ensure  => file,
      content => epp('profile/snapper/cron.daily.epp', $params),
      ;
    "/etc/${other_provider}.hourly/snapper":
      ensure  => absent,
      ;
    "/etc/${other_provider}.daily/snapper":
      ensure  => absent,
      ;
    '/etc/cron.hourly/snapper-lvm':
      ensure  => file,
      content => epp('profile/snapper/lvm-cron.hourly.epp', $params),
      ;
    '/etc/cron.daily/snapper-lvm':
      ensure  => file,
      content => epp('profile/snapper/lvm-cron.daily.epp', $params),
      ;
    '/etc/snapper/config-templates/default':
      ensure  => file,
      mode    => '0644',
      content => file('profile/snapper/default'),
      ;
    '/etc/snapper/config-templates/xfs':
      ensure  => file,
      mode    => '0644',
      content => file('profile/snapper/xfs'),
      ;
    '/usr/local/sbin/btrfs-backup-stage':
      ensure  => file,
      mode    => '0750',
      content => file('profile/snapper/btrfs-backup-stage'),
      ;
    '/usr/local/sbin/btrfs-backup-execute':
      ensure  => file,
      mode    => '0750',
      content => file('profile/snapper/btrfs-backup-execute'),
      ;
    '/usr/local/sbin/btrfs-backup-cleanup':
      ensure  => file,
      mode    => '0750',
      content => file('profile/snapper/btrfs-backup-cleanup'),
      ;
  }

  anchor { 'snapper-files': }

  create_resources('profile::role::snapper::config', $configs)

  $config_keys = sort(keys($configs)).join(' ')

  file { '/etc/sysconfig/snapper':
    ensure  => file,
    content => "SNAPPER_CONFIGS=\"${config_keys}\"\n",
    require => Package['snapper'],
  }

  $env = [
    'MAILTO=peter@verraedt.be,raymond@verraedt.be',
  ]

  cron { 'btrfs stage':
    command     => '/usr/local/sbin/btrfs-backup-stage',
    user        => 'root',
    hour        => $cron_stage_hour,
    minute      => $cron_stage_minute,
    weekday     => '6',
    environment => $env,
  }

  cron { 'btrfs execute':
    command     => '/usr/local/sbin/btrfs-backup-execute',
    user        => 'root',
    hour        => $cron_hour,
    minute      => $cron_minute,
    weekday     => '6',
    environment => $env,
  }
}
