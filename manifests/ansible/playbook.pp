define profile::ansible::playbook (
  Array $tasks,
  Boolean $execute = true,
  Array $handlers = [],
  String $host = 'localhost',
  String $remote_user = 'ansible',
  String $private_key = '',
  String $password = '',
) {
  include profile::ansible

  $connection_args = $host ? {
    'localhost' => '--connection=local',
    default => empty($private_key) ? {
      false => "--private-key ${private_key}",
      default => "--ssh-extra-args '-o PubkeyAuthentication=no'",
    },
  }

  $sanitized_name = regsubst($name, /([^a-zA-Z0-9_]+)/, '-', 'G')
  $inventory_name = regsubst("playbook_${name}", /([^a-zA-Z0-9]+)/, '_', 'G')
  $playbook_file = "/tmp/playbook-${sanitized_name}.yaml"

  concat::fragment { $name:
    target  => '/etc/ansible/hosts',
    content => "[${inventory_name}]\n${host} ansible_user=${remote_user} ansible_ssh_pass=${password}\n",
  }

  $playbook = [{
      'hosts' => $inventory_name,
      'gather_facts' => false,
      'tasks' => $tasks,
      'handlers' => $handlers,
  }]

  file { $playbook_file:
    ensure  => file,
    content => to_yaml($playbook),
  }

  $command = "/usr/bin/ansible-playbook ${connection_args} ${playbook_file}"

  if $execute {
    exec { $name:
      command     => $command,
      refreshonly => true,
      logoutput   => true,
      require     => Anchor['ansible'],
      subscribe   => File[$playbook_file],
    }
  } else {
    notify { "Please run ${playbook_file} manually when needed":
      subscribe   => File[$playbook_file],
    }
  }
}
