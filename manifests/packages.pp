class profile::packages (
  Array[String] $install = [],
) {
  package { $install:
    ensure => installed,
  }
}
