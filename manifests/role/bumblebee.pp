class profile::role::bumblebee {
  exec { 'bumblebee repo':
    command => '/usr/bin/dnf copr enable -y chenxiaolong/bumblebee',
    unless  => "/usr/bin/find /etc/yum.repos.d -name '*bumblebee.repo' | /usr/bin/grep -q bumblebee",
  }

  $packages = [
    'xorg-x11-drv-nvidia-390xx',
    'akmod-nvidia-390xx',
    'akmod-bbswitch',
    'bumblebee',
  ]

  package { $packages:
    ensure  => installed,
    require => Exec['bumblebee repo'],
    notify  => Service['bumblebeed'],
  }

  file_line { 'wayland enable gdm':
    path  => '/etc/gdm/custom.conf',
    line  => '#WaylandEnable=false',
    match => '.*WaylandEnable=.*',
  }

  file_line { 'no nvidia line in gdm rules':
    path  => '/usr/lib/udev/rules.d/61-gdm.rules',
    line  => '#DRIVER=="nvidia", RUN+="/usr/libexec/gdm-disable-wayland"',
    match => '.*DRIVER=="nvidia".*',
  }

  file_line { 'no modeset in grub':
    path   => '/etc/default/grub',
    line   => 'GRUB_CMDLINE_LINUX="quiet splash rd.driver.blacklist=nouveau modprobe.blacklist=nouveau"',
    match  => '^GRUB_CMDLINE_LINUX=.*',
    notify => Exec['regenerate grub'],
  }

  $bumblebee_config = {
    'Driver' => 'nvidia',
    'ServerGroup' => 'sysusers',
    'XorgModulePath' => '/usr/lib64/nvidia-390xx/xorg,/usr/lib64/xorg/modules',
  }

  $bumblebee_config.each |$key, $value| {
    file_line { "bumblebee ${key}":
      path    => '/etc/bumblebee/bumblebee.conf',
      line    => "${key}=${value}",
      match   => "^${key}",
      require => Package[$packages],
      notify  => Service['bumblebeed'],
    }
  }

  $file = $::boot_mode ? {
    'efi'   => '/etc/grub2-efi.cfg',
    default => '/etc/grub2.cfg',
  }

  exec { 'regenerate grub':
    command     => "/usr/sbin/grub2-mkconfig -o ${file}",
    refreshonly => true,
  }

  service { 'bumblebeed':
    ensure => running,
    enable => true,
  }

  service { 'nvidia-fallback':
    ensure => stopped,
    enable => false,
  }
}
