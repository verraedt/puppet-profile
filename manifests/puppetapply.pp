class profile::puppetapply (
  Boolean $cron = false,
  Enum['auto', 'cron', 'raid-sleep'] $cron_provider = 'auto',
  Boolean $systemd_timer = true
) {
  systemd::unit_file {
    'puppet-apply.timer':
      source => "puppet:///modules/${module_name}/puppet-apply.timer",
      enable => $systemd_timer,
      active => $systemd_timer,
      ;
    'puppet-apply.service':
      source => "puppet:///modules/${module_name}/puppet-apply.service",
      ;
  }

  service { 'puppet.service':
    ensure => stopped,
    enable => false,
  }

  package { 'deployment-scripts':
    ensure => latest, #lint:ignore:package_ensure
  }

  if $cron {
    $cron_ensure = file
  } else {
    $cron_ensure = absent
  }

  if $cron_provider == 'auto' {
    $_cron_provider = empty($facts['disks_ata_wdc']) ? {
      true => 'cron',
      default => 'raid-sleep',
    }
  } else {
    $_cron_provider = $cron_provider
  }

  $other_provider = $_cron_provider ? {
    'cron' => 'raid-sleep',
    'raid-sleep' => 'cron',
  }

  file {
    default:
      owner => 'root',
      group => 'root',
      mode  => '0755',
      ;
    "/etc/${_cron_provider}.daily/puppet":
      ensure  => $cron_ensure,
      content => "#!/bin/sh\n/usr/bin/systemctl restart puppet-apply\n",
      ;
    "/etc/${other_provider}.daily/puppet":
      ensure  => absent,
      ;
  }
}
