class profile::time {
  class { 'chrony': }

  class { 'timezone':
    timezone => 'Europe/Brussels',
  }
}
