class profile::role::server::www_user {
  include ::profile::users

  user { 'www-data':
    system => true,
    uid    => '33',
    gid    => '33',
    home   => '/var/www',
    shell  => '/usr/sbin/nologin',
  }
}
