class profile::kerberos (
  String $domain,
  Array[String] $kadmin_hostnames,
  Array[String] $kdc_hostnames,
  Array[String] $kpasswd_hostnames,
  String $master_password,
  String $realm
) {
  class { 'kerberos':
    kadmin_hostname   => $kadmin_hostnames[0],
    kdc_hostnames     => $kdc_hostnames,
    master_password   => $master_password,
    realm             => $realm,
    domain            => $domain,
    client_properties => {
      'libdefaults' => {
        'ticket_lifetime'     => '24h',
        'renew_lifetime'      => '7d',
        'forwardable'         => 'yes',
        'rdns'                => false,
        'default_ccache_name' => 'KEYRING:persistent:%{uid}',
      },
      'realms'      => {
        $realm => {
          'admin_server'  => $kadmin_hostnames,
          'passwd_server' => $kpasswd_hostnames,
        },
      },
    },
  }

  $principals = unique(["host/${::hostname}.${domain}@${realm}", "host/${::fqdn}@${realm}"])

  kerberos::keytab { '/etc/krb5.keytab':
    principals => $principals,
    wait       => 600,
    owner      => 'root',
    group      => 'root',
    mode       => '0600',
  }
}
