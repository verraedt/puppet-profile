class profile::repositories (
  Hash $yum = {},
) {
  if $facts['os']['family'] == 'RedHat' {
    create_resources('yumrepo', $yum)
  }
}
