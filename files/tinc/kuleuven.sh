#!/bin/bash

NAME=luna.kuleuven.be
EXPECTED="10.112.8.1[1-4]"

defaultgw() {
  ip -j "$@" ro show default | jq -r ".[] | [.metric,.gateway,.dev] | @tsv" | sort -n | head -n1 | cut -f2,3 | sed 's/\t/ dev /'
}

add_routes() {
  ip route replace 134.58.0.0/16 via $(defaultgw) metric 10
  ip route replace 10.0.0.0/8 via $(defaultgw) metric 10
  ip -6 route replace 2a02:2c40::/32 via $(defaultgw -6) metric 10
}

remove_routes() {
  ip route delete 134.58.0.0/16 metric 10 || true
  ip route delete 10.0.0.0/8 metric 10 || true
  ip -6 route delete 2a02:2c40::/32 metric 10 || true
}

if [[ "$(dig $NAME +short A | head -n 1)" =~ ${EXPECTED} ]]; then
  add_routes
else
  remove_routes
fi
