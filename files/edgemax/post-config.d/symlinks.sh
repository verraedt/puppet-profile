#!/bin/bash

folder_link() {
    FOLDER=$1
    TARGET=$2

    if [ ! -L $FOLDER ]; then
        if [ ! -d $TARGET ]; then
            if [ -d $FOLDER ]; then
                mv $FOLDER $TARGET
            else
                mkdir -p $TARGET
            fi
        fi
        rm -rf $FOLDER
        ln -s $TARGET $FOLDER
    fi
}

folder_link /etc/tinc /config/user-data/tinc

file_link() {
    FILE=$1
    TARGET=$2

    if [ ! -L $FILE ]; then
        mkdir -p $(dirname $TARGET)
        if [ ! -e $TARGET ]; then
            mv $FILE $TARGET
        else
            touch $TARGET
        fi
        rm -rf $FILE
        ln -s $TARGET $FILE
    fi
}

file_link /etc/dnsmasq.d/local.conf /config/user-data/dnsmasq.conf
