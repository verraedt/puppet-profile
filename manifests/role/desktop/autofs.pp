class profile::role::desktop::autofs (
  String $domain = 'net.verraedt.be',
  Array[String] $packages = [],
  Hash[String,Hash] $shares = {}
) {
  selinux::boolean { 'domain_can_mmap_files': }

  $selinux_modules = [
    'automount_ping',
    'cachefilesd_getattr',
    'cachefilesd_file',
  ]

  $selinux_modules.each |String $module| {
    selinux::module { $module:
      ensure    => present,
      source_te => "puppet:///modules/profile/selinux/${module}.te",
      builder   => 'simple',
    }
  }

  package { $packages:
    ensure => installed,
    notify => Service['autofs'],
  }

  $master = @(EOT)
    # THIS FILE IS MANAGED BY PUPPET
    # This is an automounter map and it has the following format
    # key [ -mount-options-separated-by-comma ] location
    # For details of the format look at autofs(5).
    /-    /etc/auto.direct
    /nfs  program:/etc/auto.nfs --timeout=60
    | EOT

  file {
    default:
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      require => Package[$packages],
      notify  => Service['autofs'],
      ;
    '/etc/auto.master':
      content => $master,
      ;
    '/etc/auto.direct':
      content => '',
      ;
    '/etc/auto.nfs':
      content => epp('profile/autofs-nfs.epp', { 'shares' => $shares }),
      mode    => '0755',
      ;
    '/etc/idmapd.conf':
      content => epp('profile/idmapd.conf.epp', { 'domain' => $domain }),
      notify  => Service['nfs-idmapd'],
      ;
    '/etc/cachefilesd.conf':
      content => file('profile/cachefilesd.conf'),
      notify  => Service['cachefilesd'],
      ;
    '/var/cache/fscache':
      ensure  => directory,
      mode    => '0750',
      seltype => 'cachefilesd_exec_t',
      notify  => Service['cachefilesd'],
      ;
  }

  service { 'autofs':
    ensure => running,
    enable => true,
  }

  service { 'cachefilesd':
    ensure => stopped,
    enable => false,
  }

  service { 'nfs-idmapd':
    ensure => running,
    enable => true,
    before => Service['autofs'],
  }
}
