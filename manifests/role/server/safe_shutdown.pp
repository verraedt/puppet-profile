class profile::role::server::safe_shutdown (
  Boolean $enable = false,
  Integer $timeout = 1800,
) {
  if $enable {
    $ensure_package = 'latest'
    $ensure_file = 'file'
    $ensure_service = 'running'

    Package['safe-shutdown'] ~> Service['safe-shutdown']
    File['/etc/safe-shutdown.conf'] ~> Service['safe-shutdown']
  } else {
    $ensure_package = 'absent'
    $ensure_file = 'absent'
    $ensure_service = 'stopped'

    Service['safe-shutdown'] -> Package['safe-shutdown']
  }

  package { 'safe-shutdown':
    ensure => $ensure_package,
  }

  $config = @(EOT)
    # Standby timeout in seconds
    #
    TIMEOUT=<%= $timeout %>
    | EOT

  file { '/etc/safe-shutdown.conf':
    ensure  => $ensure_file,
    content => inline_epp($config),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['safe-shutdown'],
  }

  service { 'safe-shutdown':
    ensure => $ensure_service,
    enable => $enable,
  }
}
