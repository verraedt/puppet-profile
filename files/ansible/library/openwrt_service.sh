#!/bin/sh
# Copyright (c) 2017 Markus Weippert
# GNU General Public License v3.0 (see https://www.gnu.org/licenses/gpl-3.0.txt)

source $1

changed=true
msg="Did $action"

/etc/init.d/$name $action && printf '{"changed": %s, "msg": "%s"}' "$changed" "$msg"
