class profile::role::public inherits profile::params {
  include ::profile::role::tinc
  include ::profile::role::dynamic_firewall
  include ::profile::role::consul
  include ::profile::role::server::firewalld

  firewalld_direct_rule { 'Masquerade outgoing ipv4 traffic':
    ensure        => present,
    inet_protocol => 'ipv4',
    table         => 'nat',
    chain         => 'POSTROUTING',
    priority      => 0,
    args          => "-o ${facts['networking']['primary']} -s ${profile::params::ipv4_network} -j MASQUERADE",
  }

  firewalld_direct_rule { 'Masquerade outgoing ipv6 traffic':
    ensure        => present,
    inet_protocol => 'ipv6',
    table         => 'nat',
    chain         => 'POSTROUTING',
    priority      => 0,
    args          => "-o ${facts['networking']['primary']} -s ${profile::params::ipv6_network} -j MASQUERADE",
  }
}
