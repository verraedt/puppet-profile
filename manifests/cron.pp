class profile::cron (
  Hash $jobs = {},
) {
  create_resources('cron', $jobs, { 'user' => 'root' })
}
