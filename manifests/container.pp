define profile::container (
  String $image,
  String $label = $name,
  String $docker_tag = 'latest',
  Enum['running', 'absent'] $ensure = 'running',
  Array $exposed_ports = [],
  Array $volumes = [],
  Hash $env = {},
  Hash $labels = {},
  Hash $inject = {},
  Hash $inject_loop = {},
  Optional[String] $restart_policy = undef,
  Optional[String] $command = undef,
  Optional[String] $hostname = undef,
  Optional[String] $net = undef,
  Boolean $privileged = false,
  Array $extra_parameters = [],
) {
  $_ensure = $ensure ? {
    'running' => 'present',
    default   => $ensure,
  }

  $_env = $env.map |$k,$v| {
    "${k}=${v}"
  }

  $_labels = $labels.map |$k,$v| {
    shell_escape("${k}=${v}")
  }

  if $ensure != 'absent' {
    file { "/srv/container/${name}":
      ensure  => directory,
      require => File['/srv/container'],
    }
  }

  $volumes.each |String $volume| {
    unless $volume =~ /^\// {
      $volume_name = regsubst($volume, ':.*$', '')

      if !defined(Docker_volume[$volume_name]) {
        docker_volume { $volume_name:
          driver  => 'glusterfs:latest',
          options => ["subdir=docker-${volume_name}"],
        }
      }
    }
  }

  $inject.each |String $path, Hash $item| {
    profile::container::inject { "${path} in container ${name}":
      path      => $path,
      container => $name,
      *         => $item,
    }
  }

  $inject_volumes = $inject.map |String $path, Hash $item| {
    $id = regsubst($path, '/', '-', 'G')
    "/srv/container/${name}/${id}:${path}:ro"
  }

  $inject_loop.each |String $lname, Hash $item| {
    profile::container::inject_loop { "loop ${lname} in container ${name}":
      container => $name,
      *         => $item,
    }
  }

  $inject_loop_volumes = $inject_loop.map |String $lname, Hash $loop| {
    $loop['items'].map |$item| {
      $path = profile::inline_jinja($loop['path'], { 'item' => $item })
      $id = regsubst($path, '/', '-', 'G')
      "/srv/container/${name}/${id}:${path}:ro"
    }
  }.flatten

  $_volumes = $volumes + $inject_volumes + $inject_loop_volumes

  docker::run { $name:
    ensure           => $_ensure,
    image            => "${image}:${docker_tag}",
    expose           => $exposed_ports,
    volumes          => $_volumes,
    command          => $command,
    env              => $_env,
    labels           => $_labels,
    hostname         => $hostname,
    extra_parameters => $extra_parameters,
    net              => $net,
    privileged       => $privileged,
  }
}
