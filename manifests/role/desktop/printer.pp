class profile::role::desktop::printer (
  Hash[String,String] $printers,
) {
  package { ['expect', 'cups-pdf', 'enscript']:
    ensure => installed,
    before => [
      File['/usr/local/sbin/hp-setup-plugin'],
      File['/usr/local/sbin/hp-setup-printer'],
    ],
  }

  file {
    default:
      ensure => file,
      mode   => '0700',
      owner  => 'root',
      group  => 'root',
      ;
    '/usr/local/sbin/hp-setup-plugin':
      content => file('profile/printer/setup-plugin'),
      ;
    '/usr/local/sbin/hp-setup-printer':
      content => file('profile/printer/setup-printer'),
      ;
  }

  exec { 'hp-setup-plugin':
    command => '/usr/local/sbin/hp-setup-plugin',
    creates => '/usr/share/hplip/prnt/plugins/hbpl1.so',
    require => File['/usr/local/sbin/hp-setup-plugin'],
  }

  $printers.each |String $ip, String $name| {
    exec { $ip:
      command => "/usr/local/sbin/hp-setup-printer ${ip}",
      unless  => "/usr/bin/grep \"hp:/net/${name}\" /etc/cups/printers.conf",
      require => [
        Exec['hp-setup-plugin'],
        File['/usr/local/sbin/hp-setup-printer'],
      ],
    }
  }
}
