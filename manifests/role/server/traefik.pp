class profile::role::server::traefik (
  String $consul_token = lookup('profile::role::server::caddy::consul_token'),
  String $jwt_secret = lookup('profile::role::server::caddy::jwt_secret'),
  String $digitalocean_token = lookup('profile::dns::digitalocean_token'),
  Hash[String,String] $env = {},
  Hash[String,Hash] $inject = {},
  Hash[String,Hash] $sites = {},
  Array $ports = [],
  Hash $labels = {},
  Hash $config = {},
  ) {
  $traefik = @(EOT)
  [api]
    dashboard = true

  [accessLog]

  [metrics]
    [metrics.prometheus]
      manualRouting = true

  [entryPoints]
    [entryPoints.http]
      address = ":80"

    [entryPoints.http.http]
      [entryPoints.http.http.redirections]
        [entryPoints.http.http.redirections.entryPoint]
          to = "https"
          scheme = "https"

    [entryPoints.https]
      address = ":443"

      [entryPoints.https.http.tls]
        certResolver = "leresolver"

  <%- $ports.each |$port| { %>
    [entryPoints.port-<%= $port %>]
      address = ":<%= $port %>"
  <%- } %>

  [certificatesResolvers.leresolver.acme]
    storage = "/letsencrypt/acme-<%= $facts['hostname'] %>.json"

    [certificatesResolvers.leresolver.acme.dnsChallenge]
      provider = "digitalocean"
      delayBeforeCheck = 0

  [providers]
    [providers.consulCatalog]
      exposedByDefault = false
      defaultRule = "Host(`{{ normalize .Name }}.verraedt.be`)"
      constraints = "Tag(`traefik.on=<%= $facts['hostname'] %>`) || TagRegex(`traefik\\.on=(.+,)?<%= $facts['hostname'] %>(,.+)?`) || !TagRegex(`traefik\\.on=.+`)"

      [providers.consulCatalog.endpoint]
        address = "<%= $facts['fqdn'] %>:8501"
        scheme = "https"
      [providers.consulCatalog.endpoint.tls]
        ca = "/etc/consul.d/ssl/ca.crt"

    [providers.file]
      filename = "/etc/traefik/config.yml"
  | EOT

  $_env = {
    'DO_AUTH_TOKEN' => $digitalocean_token,
    'CONSUL_TOKEN' => $consul_token,
    'SERVICE_NAME' => "traefik-${facts['hostname']}",
    'SERVICE_TAGS' => 'fw.internal,fw.external',
  } + $env

  $_inject = {
    '/etc/traefik/traefik.toml' => {
      'content' => inline_epp($traefik),
    },
    '/etc/traefik/config.yml' => {
      'content' => to_yaml($config),
    },
  } + $inject

  profile::container { 'traefik':
    image         => 'traefik',
    docker_tag    => 'v2.9',
    volumes       => [
      'traefik-ssl:/letsencrypt',
      '/etc/ssl/sensu-ca.crt:/etc/ssl/sensu-ca.crt:ro',
      '/etc/consul.d/ssl/ca.crt:/etc/consul.d/ssl/ca.crt:ro',
    ],
    net           => 'host',
    env           => $_env,
    inject        => $_inject,
    exposed_ports => ['80', '443'] + $ports,
    require       => File['/etc/ssl/sensu-ca.crt'],
    labels        => $labels,
  }
}
