class profile::devices (
  String $consul_host,
  Hash $devices,
  String $digitalocean_token,
  String $domain,
  String $external_domain,
  String $unifi_controller,
  String $unifi_hostname,
  Array $forward_rules = [],
  Array $input_rules = [],
  Hash[String,String] $ipv4_routes = {},
  Hash[String,String] $ipv6_routes = {},
  Hash[String,String] $nrpe_ping_check = {},
) inherits profile::params {
  $defaults = {
    'forward_rules' => $forward_rules,
    'input_rules' => $input_rules,
    'ipv4_routes' => $ipv4_routes,
    'ipv6_routes' => $ipv6_routes,
    'ipv4_global_network' => $profile::params::ipv4_network,
    'ipv6_global_network' => $profile::params::ipv6_network,
    'domain' => $domain,
    'unifi_hostname' => $unifi_hostname,
    'unifi_controller' => $unifi_controller,
    'consul_host' => $consul_host,
    'digitalocean_token' => $digitalocean_token,
    'external_domain' => $external_domain,
    'nrpe_ping_check' => $nrpe_ping_check,
  }
  $devices.each |String $type, Hash $values| {
    create_resources("profile::device::${type}", $values, $defaults)
  }
}
