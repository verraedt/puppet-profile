define profile::container::inject_loop (
  String $path,
  String $container,
  Array $items,
  String $jinja,
  Hash $context = {},
  String $mode = '0644',
) {
  $items.each |$item| {
    $_path = profile::inline_jinja($path, { 'item' => $item })
    $_context = $context + { 'item' => $item }

    profile::container::inject { "${_path} in container ${container}":
      path      => $_path,
      container => $container,
      jinja     => $jinja,
      context   => $_context,
      mode      => $mode,
    }
  }
}
