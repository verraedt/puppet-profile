class profile::profile {
  file {
    default:
      owner => 'root',
      group => 'root',
      mode  => '0644',
      ;
    '/etc/profile.d/path.sh':
      source => "puppet:///modules/${module_name}/profile.d/path.sh",
      ;
    '/etc/profile.d/userdirs.sh':
      source => "puppet:///modules/${module_name}/profile.d/userdirs.sh",
      ;
    '/etc/profile.d/history.sh':
      source => "puppet:///modules/${module_name}/profile.d/history.sh",
      ;
    '/etc/xdg/user-dirs.conf':
      source => "puppet:///modules/${module_name}/user-dirs.conf",
      ;
    '/usr/local/bin/cleanup-recently-used':
      source => "puppet:///modules/${module_name}/cleanup-recently-used.py",
      mode   => '0755',
      ;
  }
}
