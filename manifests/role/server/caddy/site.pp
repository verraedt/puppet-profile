define profile::role::server::caddy::site (
  Array[String] $domains = [$name],
  String $logfolder = $name.regsubst(/\//, '-', 'G'),
  String $config = '',
  Optional[String] $ssl_target = undef, # Unused
) {
  $indent_config = $config.regsubst(/^/, '  ', 'G').regsubst(/^\s*$/, '', 'G')
  $site_config = @(EOT)
    <%= join($domains, ' ') %> {                     
    
    <%- if $auto_tls { -%>                         
      tls {                                          
        dns digitalocean                             
        key_type rsa4096                             
      }                                              
    <%- } -%>                                      

      header / {                                     
        Strict-Transport-Security "max-age=31536000;"
      }                                              

    <%= $indent_config %>

      log / /var/log/caddy/<%= $logfolder %>/access.log "{combined} {~jwt_token}" {
        rotate_size 50
        rotate_age 90
        rotate_keep 5
        rotate_compress
      }

      errors /var/log/caddy/<%= $logfolder %>/error.log {
        rotate_size 50
        rotate_age 90
        rotate_keep 5
        rotate_compress
      }
    }
    | EOT

  concat::fragment { $name:
    target  => '/etc/caddy/Caddyfile',
    content => inline_epp($site_config),
    order   => $name.regsubst(/[\/:]/, '', 'G'),
  }

  file { "/var/log/caddy/${logfolder}":
    ensure => directory,
    owner  => 'root',
    group  => 'www-data',
    mode   => '0775',
    before => Profile::Container['caddy'],
  }
}
