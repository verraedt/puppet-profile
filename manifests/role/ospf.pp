class profile::role::ospf (
  String $password,
  String $router_id,
  String $anycast_ipv4_network = '172.16.226.0/24',
  String $area = '0.0.0.0',
  String $hostname = $facts['hostname'],
  String $ipv4_network = '172.16.0.0/16',
  String $ipv6_network = 'fd00:76b8:b991::/48',
  Boolean $use_frr = $facts['os']['name'] == 'AlmaLinux',
  Boolean $only_localnet = false,
  Boolean $redistribute_connected = false,
  Boolean $redistribute_static = false,
  Hash[String,String] $static_routes = {},
) inherits profile::params {
  $daemons = ['zebra', 'ospfd', 'ospf6d']

  $params = {
    'hostname' => $hostname,
    'password' => $password,
    'interfaces' => $profile::params::interfaces,
    'anycast_ipv4_network' => $anycast_ipv4_network,
    'router_id' => $router_id,
    'area' => $area,
    'package' => $package,
    'redistribute_connected' => $redistribute_connected,
    'redistribute_static' => $redistribute_static,
    'static_routes' => $static_routes,
    'ipv4_network' => $ipv4_network,
    'ipv6_network' => $ipv6_network,
    'only_localnet' => $only_localnet,
  }

  if $use_frr {
    $package = 'frr'
    $services = ['frr']

    package { 'quagga':
      ensure => absent,
    }

    $daemons.each |String $key| {
      file_line { $key:
        ensure  => present,
        path    => "/etc/${package}/daemons",
        line    => "${key}=yes",
        match   => "^${key}=",
        require => Package[$package],
        notify  => Service[$services],
      }
    }

    file { "/etc/${package}/vtysh.conf":
      content => '',
      owner   => $package,
      group   => 'frrvty',
      mode    => '0640',
      require => Package[$package],
      notify  => Service[$services],
    }

    file { "/etc/${package}/frr.conf":
      content => epp("profile/quagga/frr.conf.epp", $params),
      owner   => $package,
      group   => $package,
      mode    => '0600',
      require => Package[$package],
      notify  => Service[$services],
    }
  } else {
    $package = 'quagga'
    $services = ['zebra', 'ospfd', 'ospf6d']

    $daemons.each |String $key| {
      file { "/etc/${package}/${key}.conf":
        content => epp("profile/quagga/${key}.conf.epp", $params),
        owner   => $package,
        group   => $package,
        mode    => '0600',
        require => Package[$package],
        notify  => Service[$key],
      }
    }
  }

  package { $package:
    ensure => installed,
  }

  service { $services:
    ensure  => running,
    enable  => true,
    require => Package[$package],
  }

  $systemd_override = @(EOT)
    [Unit]
    After=shorewall.service
    After=shorewall6.service
    | EOT

  $services.each |String $service| {
    systemd::dropin_file { $service:
      filename => 'override.conf',
      unit     => "${service}.service",
      content  => $systemd_override,
      notify   => Service[$service],
    }
  } 
}
