class profile::keyboard (
  Optional[String] $layout = 'be',
) {
  $keyboard_config = {
    'XKBLAYOUT' => $layout,
    'BACKSPACE' => 'guess',
  }

  $keyboard_config_settings = {
    'quote_char' => '',
  }

  file { '/etc/default/keyboard':
    ensure  => file,
    content => hash2kv($keyboard_config, $keyboard_config_settings),
  }
}
