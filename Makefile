DEV_IMAGE="registry.icts.kuleuven.be/icts/puppet-server-base-dev:latest"
ifeq (, $(shell which podman))
  CONTAINER_RUN=docker run -u $(shell id -u):$(shell id -g)
else
  CONTAINER_RUN=podman run
endif
RUN=$(CONTAINER_RUN) --rm $(RUN_WITH_TTY) -w /data \
  -v '$(PWD)':/data

test:
	$(RUN) $(DEV_IMAGE) rake test

lint:
	$(RUN) $(DEV_IMAGE) rake lint

fix-lint:
	$(RUN) -e PUPPET_LINT_FIX=yes $(DEV_IMAGE) rake lint
