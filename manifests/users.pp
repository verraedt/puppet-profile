class profile::users {
  group { 'docker':
    system => true,
  }

  group { 'www-data':
    system => true,
    gid    => 33,
  }
}
