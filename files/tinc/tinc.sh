#!/bin/bash

NAME=wifi.verraedt.be
EXPECTED=172.16.226.8
NETWORK=verraedt

stop_tinc() {
  if [ -f /var/run/tinc.${NETWORK}.pid ]; then
    systemctl stop "tinc@${NETWORK}"
  fi
}

start_tinc() {
  if [ ! -f /var/run/tinc.${NETWORK}.pid ]; then
    systemctl start "tinc@${NETWORK}"
  fi
}

if [[ "$(dig $NAME +short A)" == "${EXPECTED}" ]]; then
  stop_tinc
elif curl -sSI --max-time 2 https://verraedt.jfrog.io >/dev/null; then
  start_tinc
fi
