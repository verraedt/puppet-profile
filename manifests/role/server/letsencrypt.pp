class profile::role::server::letsencrypt (
  String $ssl_target = '/etc/ssl/letsencrypt',
  Hash[String,String] $certificates = {},
) {
  file { $ssl_target:
    ensure => directory,
  }

  $certificates.each |String $domain, String $host| {
    file {
      default:
        ensure  => file,
        mode    => '0600',
        owner   => 'root',
        group   => 'root',
        ;
      "${ssl_target}/${domain}":
        ensure => directory,
        mode   => '0700',
        ;
    }

    $key = "${ssl_target}/${domain}/${domain}.key"
    $crt = "${ssl_target}/${domain}/${domain}.crt"

    exec { $crt:
      command => ['/usr/local/bin/traefik-acme', '-r', 'leresolver', '-a', "/mnt/vol1/docker-traefik-ssl/acme-${host}.json", '-c', $crt, '-k', $key, $domain], 
      cwd     => "${ssl_target}/${domain}",
      require => [
        Mount['/mnt/vol1'],
        Archive['/root/traefik-acme.tar.gz'],
      ],
    }
    
    exec { "ISRG-Root-X1.sh ${domain}":
      command => "/usr/local/bin/ISRG-Root-X1.sh ${domain}.crt",
      unless  => "/usr/local/bin/ISRG-Root-X1.sh --check ${domain}.crt",
      cwd     => "${ssl_target}/${domain}",
      require => [
        Exec[$crt],
        File['/usr/local/bin/ISRG-Root-X1.sh'],
      ],
    }
  }

  archive { '/root/traefik-acme.tar.gz':
    ensure        => present,
    extract       => true,
    extract_path  => '/usr/local/bin',
    source        => 'https://github.com/na4ma4/traefik-acme/releases/download/v0.3.0/traefik-acme_0.3.0_linux_amd64.tar.gz',
    checksum      => 'd1971c251a469a83f617fb528a4babf24ea03a12b9722f51a45ff3502949299c',
    checksum_type => sha256,
    creates       => '/usr/local/bin/traefik-acme',
    cleanup       => true,
  }

  file { '/usr/local/bin/ISRG-Root-X1.sh':
    ensure  => file,
    content => file('profile/ISRG-Root-X1.sh'),
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
  }
}
