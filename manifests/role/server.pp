class profile::role::server {
  include ::profile::role::server::hosts
  include ::profile::role::server::traefik
  include ::profile::role::server::caddy
  include ::profile::role::server::letsencrypt
  include ::profile::role::server::backup
  include ::profile::role::server::raid_sleep
  include ::profile::role::server::docker
  include ::profile::role::server::firewalld
  include ::profile::role::server::fail2ban
  include ::profile::role::server::safe_shutdown
  include ::profile::role::server::node_exporter
  include ::profile::role::server::cadvisor
  include ::profile::role::server::portainer
  include ::profile::sensu::agent
  include ::profile::role::desktop::autofs 
  include ::profile::role::glusterfs

  service { 'dnf-makecache.timer':
    ensure => stopped,
    enable => false,
  }
}
