class profile::dns (
  String $external_domain,
  Optional[String] $digitalocean_token = undef,
  Optional[String] $public_interface = undef
) {
  package { 'update-dns':
    ensure => latest, #lint:ignore:package_ensure
  }

  if !empty($digitalocean_token) {
    exec { 'update dns':
      command => inline_epp('/usr/bin/update-dns <% if $public_interface { %>--interface <%= $public_interface %> <% } %>--do-token <%= $digitalocean_token %> --do-domain <%= $external_domain %>'),
      require => Package['update-dns'],
    }
  }
}
