if [ -z "$PATH" ]; then
  if [ `id -u` -eq 0 ]; then
    PATH="/usr/sbin:/usr/bin:/sbin:/bin"
  else
    PATH="/usr/bin:/bin"
  fi
fi

if [ `id -u` -eq 0 ]; then
  PATH="/usr/local/sbin:/usr/local/bin:$PATH"
else
  PATH="/usr/local/bin:$PATH"
fi

# Cleanup path
if [ -n "$PATH" ]; then
    old_PATH=$PATH:; PATH=
    while [ -n "$old_PATH" ]; do
        x=${old_PATH%%:*}       # the first remaining entry
        case $PATH: in
            *:"$x":*) ;;         # already there
            *) PATH=$PATH:$x;;    # not there yet
        esac
        old_PATH=${old_PATH#*:}
    done
    PATH=${PATH#:}
    unset old_PATH x
fi

export PATH
