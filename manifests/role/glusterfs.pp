class profile::role::glusterfs (
  Array[String] $peers,
) {
  if ($facts['os']['family'] == 'RedHat' and versioncmp($facts['os']['release']['major'],'8') == 0) {
    package { 'centos-release-storage-common':
      ensure => present,
      before => Class['gluster'],
    }

    package { 'python3-pyxattr':
      ensure          => installed,
      install_options => [
        '--enablerepo', 'powertools',
      ],
    }

    yumrepo { 'gluster':
      ensure   => present,
      baseurl  => 'http://mirror.centos.org/centos/8-stream/storage/x86_64/gluster-9',
      enabled  => true,
      gpgcheck => true,
      gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-Storage',
    }
  } else {
    package { 'centos-release-gluster9':
      ensure => present,
      before => Class['gluster'],
    }
  }

  $server = $facts['fqdn'] in $peers

  class { 'gluster':
    server         => $server,
    client         => true,
    repo           => false,
    pool           => 'production',
    server_package => 'glusterfs-server',
    client_package => 'glusterfs-fuse',
  }

  if $server {
    $peers.each |String $peer| {
      if ($peer != $facts['fqdn']) {
        gluster::peer { $peer:
          pool => 'production',
          fqdn => $peer,
        }
      }
    }

    $mount_host = $facts['fqdn']
  } else {
    $mount_host = $peers[0]
  }

  file { '/mnt/vol1':
    ensure => directory,
  }
  -> mount { '/mnt/vol1':
    ensure => mounted,
    atboot => false,
    device => "${mount_host}:/vol1",
    fstype => 'glusterfs',
  }
}
