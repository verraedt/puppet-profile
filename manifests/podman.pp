class profile::podman {
  package { 'podman':
    ensure => present,
  }

  sysctl { 'user.max_user_namespaces':
    ensure => present,
    value  => '10000',
  }

  file {
    default:
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => "peter:100000:65536\n",
      ;
    '/etc/subuid':
      ;
    '/etc/subgid':
      ;
  }
}
