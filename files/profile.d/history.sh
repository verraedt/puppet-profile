if [ ! -z "$HOME" ] && [ -f $HOME/.local/share/recently-used.xbel ]; then
  /usr/local/bin/cleanup-recently-used $HOME/.local/share/recently-used.xbel
fi
