define profile::ansible::task (
  String $module = $name,
  Hash $data = {},
) {
  $tasks = [{
      name => $name,
      $module => $data,
  }]

  profile::ansible::playbook { $name:
    tasks => $tasks,
  }
}
