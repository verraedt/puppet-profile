class profile::ldap (
  String $ca_cert = lookup('ssl::ldap::ca'),
  String $ldap_base = 'dc=verraedt,dc=be',
  String $ldap_uri = 'ldaps://ldap.net.verraedt.be',
  String $realm = lookup('profile::kerberos::realm'),
) {
  if $facts['os']['family'] == 'RedHat' {
    package { 'openldap-clients':
      ensure => installed,
      before => [
        File['/etc/openldap/ca.crt'],
        File['/etc/openldap/ldap.conf'],
      ],
    }

    $directory = '/etc/openldap'
  } else {
    $directory = '/etc/ldap'
  }

  $ldap_config = @(EOT)
    # LDAP Defaults
    #   
      
    # See ldap.conf(5) for details
    # This file should be world readable but not world writable.
        
    BASE <%= $ldap_base %>
    URI <%= $ldap_uri %>

    SASL_MECH GSSAPI
    SASL_REALM <%= $realm %>
            
    #SIZELIMIT  12
    #TIMELIMIT  15
    #DEREF      never

    # TLS certificates (needed for GnuTLS)
    TLS_CACERT  <%= $directory %>/ca.crt

    #TLSVerifyClient demand
    | EOT

  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      ;
    "${directory}/ca.crt":
      content => $ca_cert,
      ;
    "${directory}/ldap.conf":
      content => inline_epp($ldap_config),
      ;
  }
}
