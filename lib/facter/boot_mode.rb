Facter.add('boot_mode') do
  setcode do
    if Dir.exist? ('/sys/firmware/efi')
      'efi'
    else
      'legacy'
    end
  end
end
