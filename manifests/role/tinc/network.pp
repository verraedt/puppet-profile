define profile::role::tinc::network (
  String $local_hostname,
  Array[String] $bind_to = [],
  Array[String] $connect_to = [],
  String $vpn_name = $name,
  String $target_directory = '/etc/tinc',
  Boolean $toggle_br0 = false,
  Optional[String] $toggle_ipv4_address = undef,
  Optional[String] $toggle_ipv6_address = undef,
  Hash[String,Hash[String,String]] $hosts = {},
) {
  if $toggle_ipv4_address {
    $local_ipv4_address = sprintf('%s/32', ip_address($toggle_ipv4_address))
  } else {
    $local_ipv4_address = ''
  }
  if $toggle_ipv6_address {
    $local_ipv6_address = sprintf('%s/128', ip_address($toggle_ipv6_address))
  } else {
    $local_ipv6_address = ''
  }

  $params = {
    'toggle_ipv4_address' => $toggle_ipv4_address,
    'toggle_ipv6_address' => $toggle_ipv6_address,
    'local_ipv4_address' => $local_ipv4_address,
    'local_ipv6_address' => $local_ipv6_address,
  }

  file {
    default:
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      ;
    "${target_directory}/${vpn_name}":
      ensure => directory,
      mode   => '0755',
      ;
    "${target_directory}/${vpn_name}/hosts":
      ensure => directory,
      mode   => '0755',
      ;
    "${target_directory}/${vpn_name}/tinc.conf":
      content => epp('profile/tinc/tinc.conf.epp', { 'hostname' => $local_hostname, 'connect_to' => $connect_to, 'bind_to' => $bind_to }),
      ;
    "${target_directory}/${vpn_name}/tinc-up":
      content => epp('profile/tinc/tinc-up.epp', $params),
      mode    => '0754',
      ;
    "${target_directory}/${vpn_name}/tinc-down":
      content => epp('profile/tinc/tinc-down.epp', $params),
      mode    => '0754',
      ;
  }

  $hosts.each |String $hostname, Hash $data| {
    profile::role::tinc::host { "${target_directory} host ${hostname}":
      vpn_name         => $vpn_name,
      local_hostname   => $local_hostname,
      hostname         => $hostname,
      target_directory => $target_directory,
      *                => $data,
    }
  }
}
