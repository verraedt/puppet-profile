if [ ! -z "$HOME" ] && [ ! -z "$UID" ] && [ $UID -ne 0 ]; then
  mkdir -p $HOME/.config $HOME/Bureaublad $HOME/Downloads $HOME/Sjablonen

  SERVER=$(groups | grep -ow 'data-.*' | cut -d- -f2 | head -n1)

  if [ -z "$SERVER" ]; then
    mkdir -p $HOME/Documenten $HOME/Muziek $HOME/Afbeeldingen $HOME/Video\'s $HOME/Publiek
    cat >$HOME/.config/user-dirs.dirs <<EOF
# This file is written by /etc/profile.d/userdirs.sh
# If you want to change or add directories, just edit the line you're
# interested in. All local changes will be retained on the next run
# Format is XDG_xxx_DIR="\$HOME/yyy", where yyy is a shell-escaped
# homedir-relative path, or XDG_xxx_DIR="/yyy", where /yyy is an
# absolute path. No other format is supported.
# 
XDG_DESKTOP_DIR="\$HOME/Bureaublad"
XDG_DOWNLOAD_DIR="\$HOME/Downloads"
XDG_TEMPLATES_DIR="\$HOME/Sjablonen"
XDG_DOCUMENTS_DIR="\$HOME/Documenten"
XDG_MUSIC_DIR="\$HOME/Muziek"
XDG_PICTURES_DIR="\$HOME/Afbeeldingen"
XDG_VIDEOS_DIR="\$HOME/Video's"
XDG_PUBLICSHARE_DIR="\$HOME/Publiek"
EOF
  else
    cat >$HOME/.config/user-dirs.dirs <<EOF
# This file is written by /etc/profile.d/userdirs.sh
# If you want to change or add directories, just edit the line you're
# interested in. All local changes will be retained on the next run
# Format is XDG_xxx_DIR="\$HOME/yyy", where yyy is a shell-escaped
# homedir-relative path, or XDG_xxx_DIR="/yyy", where /yyy is an
# absolute path. No other format is supported.
# 
XDG_DESKTOP_DIR="\$HOME/Bureaublad"
XDG_DOWNLOAD_DIR="\$HOME/Downloads"
XDG_TEMPLATES_DIR="\$HOME/Sjablonen"
XDG_DOCUMENTS_DIR="/nfs/${SERVER}/private/${USER}/Documenten"
XDG_MUSIC_DIR="/nfs/${SERVER}/shared/Muziek"
XDG_PICTURES_DIR="/nfs/${SERVER}/shared/Afbeeldingen"
XDG_VIDEOS_DIR="/nfs/${SERVER}/shared/Video's"
XDG_PUBLICSHARE_DIR="/nfs/${SERVER}/shared/Documenten"
EOF
  fi
fi
