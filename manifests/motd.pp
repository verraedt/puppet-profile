class profile::motd {
  $classes = lookup('classes', Array[String], 'unique') - lookup('exclude_classes', Array[String], 'unique')

  file { '/etc/motd':
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('profile/motd.epp', { 'classes' => $classes }),
  }
}
