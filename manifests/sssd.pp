class profile::sssd (
  Hash[String,Any] $config = {},
  Hash[String,Hash[String,Any]] $domains = {},
  Hash[String,Any] $nss_config = {},
  Hash[String,Any] $pam_config = {}
) {
  include ::profile::users

  $sssd_config = {
    'sssd' => merge($config, {
        'domains'  => keys($domains),
        'services' => 'nss,pam,ssh',
    }),
    'nss' => $nss_config,
    'pam' => $pam_config,
  }

  $my_domains = Hash($domains.map |$k, $v| { $r = ["domain/${k}", $v] })

  $my_config = merge(
    $sssd_config,
    $my_domains
  )

  class { '::sssd':
    config => $my_config,
  }

  # Sudoers & policykit configuration
  file {
    default:
      ensure => directory,
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
      ;
    '/etc/sudoers.d':
      mode   => '0750',
      ;
    '/etc/sudoers.d/ldap':
      ensure  => file,
      mode    => '0440',
      content => "%sysadmins ALL=(ALL) NOPASSWD: ALL\nDefaults:%sysadmins !requiretty\n",
      ;
    '/etc/polkit-1':
      ;
    '/etc/polkit-1/localauthority.conf.d':
      ;
    '/etc/polkit-1/localauthority.conf.d/52-ldap-admin.conf':
      ensure  => file,
      mode    => '0644',
      content => "[Configuration]\nAdminIdentities=unix-group:sudo;unix-group:admin;unix-group:sysadmins\n",
      ;
    '/etc/security/group.conf':
      ensure  => file,
      mode    => '0644',
      content => file('profile/security-group.conf'),
      ;
  }

  # Pam modules
  package { 'libcgroup-pam':
    ensure => installed,
  }

  $pam_files = ['sshd', 'login', 'su', 'runuser']

  $pam_files.each |String $pam_file| {
    file_line { "/etc/pam.d/${pam_file} - pam_mkhomedir":
      path  => "/etc/pam.d/${pam_file}",
      line  => 'session required pam_mkhomedir.so skel=/etc/skel umask=0022',
      match => 'pam_mkhomedir',
    }

    file_line { "/etc/pam.d/${pam_file} - pam_group":
      path  => "/etc/pam.d/${pam_file}",
      line  => 'auth required pam_group.so',
      match => 'pam_group',
      after => '.*',
    }
  }
}
