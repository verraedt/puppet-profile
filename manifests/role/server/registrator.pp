class profile::role::server::registrator (
  String $consul_token,
  Optional[String] $digitalocean_token = lookup('profile::dns::digitalocean_token'),
  String $lb_interface,
  Array[String] $public_addresses = [],
  Optional[String] $public_interface = undef
) inherits profile::params {
  package { 'registrator':
    ensure => latest, #lint:ignore:package_ensure
    notify => Service['registrator'],
  }

  package { 'ipvsadm':
    ensure => present,
    before => Service['registrator'],
  }

  $config = @(EOT)
    DIGITALOCEAN_TOKEN=<%= $digitalocean_token %>
    OPTIONS="<% if $public_interface { %> --interface <%= $public_interface %><% } %><% $public_addresses.each |$address| { %> --address <%= $address %><% } %> --lb-interface <%= $lb_interface %>"
    CONSUL_HTTP_TOKEN="<%= $consul_token %>"
    | EOT

  file { '/etc/sysconfig/registrator':
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    content => inline_epp($config),
    require => Package['registrator'],
    notify  => Service['registrator'],
  }

  service { 'registrator':
    ensure => running,
    enable => true,
    before => [
      Service['docker'],
      Service['dynamic-firewall'],
    ],
  }
}
