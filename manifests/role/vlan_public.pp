class profile::role::vlan_public (
  String $ipv4_address,
  String $ipv4_gateway,
  String $ipv6_gateway,
  String $device = $facts['networking']['primary'],
  String $routing_table = 'vlan50',
  Integer $vlan = 50,
) inherits profile::params {
  include ::profile::role::server::firewalld

  if ($facts['os']['family'] == 'RedHat' and versioncmp($facts['os']['release']['major'],'8') >= 0) {
    package { 'NetworkManager-config-routing-rules':
      ensure => installed,
    }

    service { 'NetworkManager-dispatcher':
      ensure  => running,
      enable  => true,
      require => Package['NetworkManager-config-routing-rules'],
    }

    Service['NetworkManager-dispatcher'] -> Network::Rule <| |>
    Service['NetworkManager-dispatcher'] -> Network::Route <| |>
  }

  network::routing_table { $routing_table:
    table_id => "${vlan}",
  }

  network::interface { "${device}.${vlan}":
    ipaddress            => ip_address($ipv4_address),
    netmask              => ip_netmask($ipv4_address),
    defroute             => 'no',
    ipv6init             => 'yes',
    gateway              => $ipv4_gateway,
    ipv6_defaultgw       => $ipv6_gateway,
    ipv6_autoconf        => 'yes',
    onparent             => $device,
    type                 => 'VLAN',
    vlan                 => 'yes',
    vlan_id              => $vlan,
    zone                 => 'public',
    options_extra_redhat => { 'IPV6_DEFROUTE' => 'no' },
    notify               => Exec["ifup-${device}.${vlan}"],
  }

  network::rule { "${device}.${vlan}":
    ensure  => present,
    iprule  => [
      "from ${ipv4_address} lookup ${routing_table}",
      "to ${ipv4_address} lookup ${routing_table}",
      "from 2000::/3 to 2000::/3 lookup ${routing_table} pref 100", # Just route all public traffic over the vlan
    ] + $internal_public_ip6.map |$v| { "from ${v} lookup main pref 10" }, # Add rules for semi static public ipv6 addresses assigned to internal interface (cfr edpnet)
    family  => ['inet4', 'inet4', 'inet6'] + $internal_public_ip6.map |$v| { 'inet6' },
    require => Network::Routing_table[$routing_table],
    notify  => Exec["ifup-${device}.${vlan}"],
  }

  network::route { "${device}.${vlan}":
    ensure    => present,
    ipaddress => ['0.0.0.0', '::'],
    netmask   => ['0.0.0.0', '0'],
    cidr      => ['0', '0'],
    gateway   => [$ipv4_gateway, $ipv6_gateway],
    table     => [$routing_table, $routing_table],
    family    => ['inet4', 'inet6'],
    require   => Network::Routing_table[$routing_table],
    notify    => Exec["ifup-${device}.${vlan}"],
  }

  exec { "ifup-${device}.${vlan}":
    command     => "/sbin/ifup ${device}.${vlan}",
    refreshonly => true,
  }
}
