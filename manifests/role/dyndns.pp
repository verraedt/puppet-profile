class profile::role::dyndns (
  Optional[String] $digitalocean_token = lookup('profile::dns::digitalocean_token'),
  String $external_domain = lookup('profile::dns::external_domain'),
  String $password = 'noip',
  String $username = 'noip'
) {
  package { 'dyndns':
    ensure => latest, #lint:ignore:package_ensure
    notify => Service['dyndns'],
  }

  $hash = sha256($password)
  $config = @(EOT)
    USERNAME=<%= $username %>
    PASSWORD_HASH=<%= $hash %>
    DIGITALOCEAN_TOKEN=<%= $digitalocean_token %>
    | EOT

  file { '/etc/sysconfig/dyndns':
    ensure  => file,
    content => inline_epp($config),
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    require => Package['dyndns'],
    notify  => Service['dyndns'],
  }

  service { 'dyndns':
    ensure => running,
    enable => true,
  }
}
