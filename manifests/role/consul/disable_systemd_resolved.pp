class profile::role::consul::disable_systemd_resolved {
  service { 'systemd-resolved':
    ensure => running,
    enable => true,
    before => Service['consul'],
  }

  $nm_config = @(EOT)
    [main]
    systemd-resolved=false
    dns=none
    | EOT

  file { '/etc/NetworkManager/conf.d/no-systemd-resolved.conf':
    ensure  => file,
    content => $nm_config,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    before  => Service['systemd-resolved'],
  }

  $resolved_config = @(EOT)
    [Resolve]
    #FallbackDNS=
    #Domains=
    #LLMNR=yes
    #MulticastDNS=yes
    #DNSSEC=allow-downgrade
    #DNSOverTLS=no
    #Cache=yes
    #DNSStubListener=udp
    DNSStubListener=no
    | EOT

  file { '/etc/systemd/resolved.conf':
    ensure  => file,
    content => $resolved_config,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    before  => Service['systemd-resolved'],
  }
}
