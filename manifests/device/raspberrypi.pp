define profile::device::raspberrypi (
  String $consul_host,
  String $digitalocean_token,
  String $domain,
  String $external_domain,
  String $unifi_controller,
  String $unifi_hostname,
  Array $forward_rules,
  Array $input_rules,
  Hash[String,String] $ipv4_routes,
  Hash[String,String] $ipv6_routes,
  Hash[String,String] $nrpe_ping_check,
  String $ipv4_global_network,
  String $ipv6_global_network,
  String $address,
  String $goarch,
  String $consul_ca_cert = lookup('ssl::consul::ca'),
  Optional[String] $consul_cert = undef,
  Optional[String] $consul_key = undef,
  String $consul_node_name = "${name}.net.verraedt.be",
  Optional[String] $radius_cert = undef,
  Optional[String] $radius_key = undef,
  Array $services = [],
  Array $projects = [],
) {
  $_consul_cert = empty($consul_cert) ? {
    true => lookup("ssl::consul::certs.'${consul_node_name}'.certificate"),
    default => $consul_cert,
  }
  $_consul_key = empty($consul_key) ? {
    true => lookup("ssl::consul::certs.'${consul_node_name}'.private_key"),
    default => $consul_key,
  }

  $_radius_cert = empty($radius_cert) ? {
    true => lookup("ssl::radius::certs.'${name}'.certificate"),
    default => $radius_cert,
  }
  $_radius_key = empty($radius_key) ? {
    true => lookup("ssl::radius::certs.'${name}'.private_key"),
    default => $radius_key,
  }

  $sanitized_name = regsubst($name, /([^a-zA-Z0-9-]+)/, '-', 'G')

  $tasks = [
    {
      raw => 'apk add python',
    },
    {
      name   => 'consul ssl',
      copy   => {
        dest    => '/etc/consul.d/ssl/{{ item.key }}',
        owner   => 'root',
        group   => 'root',
        mode    => '0600',
        content => '{{ item.value }}',
      },
      with_dict => {
        'ca.crt' => $consul_ca_cert,
        "${consul_node_name}.crt" => $_consul_cert,
        "${consul_node_name}.key" => $_consul_key,
      },
      notify => ['restart consul'],
    },
    {
      name   => 'wpa_supplicant ssl',
      copy   => {
        dest    => '/etc/wpa_supplicant/{{ item.key }}',
        owner   => 'root',
        group   => 'root',
        mode    => '0600',
        content => '{{ item.value }}',
      },
      with_dict => {
        "${name}.crt" => $_radius_cert,
        "${name}.key" => $_radius_key,
      },
    }
  ]

  $handlers = [
    {
      name => 'restart consul',
      service => {
        name  => 'consul',
        state => 'restarted',
      }
    },
  ]

  profile::ansible::playbook { $name:
    host        => $address,
    remote_user => 'root',
    private_key => '/root/.ssh/id_rsa_ansible',
    tasks       => $tasks,
    handlers    => $handlers,
    execute     => false,
  }
}
