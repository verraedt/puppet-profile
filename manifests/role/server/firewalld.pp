class profile::role::server::firewalld (
  Array[String] $external = [],
  Hash[String,Array[String]] $external_rich = {},
  Array[String] $internal = [],
  Hash[String,Array[String]] $internal_rich = {}
) inherits profile::params {
  include ::firewalld

  service { 'shorewall':
    ensure => stopped,
    enable => false,
  }

  service { 'shorewall6':
    ensure => stopped,
    enable => false,
  }

  firewalld_zone { 'public':
    ensure           => present,
    target           => '%%REJECT%%',
    purge_rich_rules => true,
    purge_services   => true,
    purge_ports      => true,
    interfaces       => $profile::params::public_interfaces,
  }

  firewalld_zone { 'internal':
    ensure           => present,
    target           => '%%REJECT%%',
    purge_rich_rules => true,
    purge_services   => true,
    purge_ports      => true,
    interfaces       => $profile::params::internal_interfaces,
  }

  firewalld_zone { 'trusted':
    ensure     => present,
    target     => 'ACCEPT',
    interfaces => $profile::params::trusted_interfaces,
  }

  firewalld_custom_service { 'ospf':
    ensure      => present,
    short       => 'ospf',
    description => 'OSPF route exchange protocol',
    protocols   => ['ospf'],
    ports       => [],
  }

  firewalld_direct_rule { 'ipv4 icmp':
    ensure        => present,
    inet_protocol => 'ipv4',
    table         => 'filter',
    chain         => 'INPUT',
    priority      => 1,
    args          => '-p icmp -j ACCEPT',
  }

  firewalld_direct_rule { 'ipv4 icmp fwd':
    ensure        => present,
    inet_protocol => 'ipv4',
    table         => 'filter',
    chain         => 'FORWARD',
    priority      => 1,
    args          => '-p icmp -j ACCEPT',
  }

  firewalld_direct_rule { 'ipv6 icmp':
    ensure        => present,
    inet_protocol => 'ipv6',
    table         => 'filter',
    chain         => 'INPUT',
    priority      => 1,
    args          => '-p ipv6-icmp -j ACCEPT',
  }

  firewalld_direct_rule { 'ipv6 icmp fwd':
    ensure        => present,
    inet_protocol => 'ipv6',
    table         => 'filter',
    chain         => 'FORWARD',
    priority      => 1,
    args          => '-p ipv6-icmp -j ACCEPT',
  }

  firewalld_ipset {
    default:
      ensure => present,
      type   => 'hash:ip,port',
      ;
    'DYNAMIC-INTERNAL':
      ;
    'DYNAMIC-EXTERNAL':
      ;
    'DYNAMIC-INTERNAL-6':
      options => { 'family' => 'inet6' },
      ;
    'DYNAMIC-EXTERNAL-6':
      options => { 'family' => 'inet6' },
      ;
  }

  $external.each |String $service| {
    firewalld_service { "Allow ${service} from the public zone":
      ensure  => present,
      service => $service,
      zone    => 'public',
    }
  }

  $internal.each |String $service| {
    firewalld_service { "Allow ${service} from the internal zone":
      ensure  => present,
      service => $service,
      zone    => 'internal',
    }
  }

  $external_rich.each |String $ip, Array $services| {
    if $ip =~ /:/ {
      $family = 'ipv6'
    } else {
      $family = 'ipv4'
    }

    $services.each |String $service| {
      firewalld_rich_rule { "Accept ${service} on ${ip} from the public zone":
        ensure  => present,
        zone    => 'public',
        dest    => $ip,
        service => $service,
        action  => 'accept',
        family  => $family,
      }
    }
  }

  $internal_rich.each |String $ip, Array $services| {
    if $ip =~ /:/ {
      $family = 'ipv6'
    } else {
      $family = 'ipv4'
    }

    $services.each |String $service| {
      firewalld_rich_rule { "Accept ${service} on ${ip} from the internal zone":
        ensure  => present,
        zone    => 'internal',
        dest    => $ip,
        service => $service,
        action  => 'accept',
        family  => $family,
      }
    }
  }

  firewalld_direct_rule { 'External services ipv4':
    ensure        => present,
    inet_protocol => 'ipv4',
    table         => 'filter',
    chain         => 'IN_public_allow',
    priority      => 0,
    args          => '-m set --match-set DYNAMIC-EXTERNAL dst,dst -j ACCEPT',
  }

  firewalld_direct_rule { 'External services ipv6':
    ensure        => present,
    inet_protocol => 'ipv6',
    table         => 'filter',
    chain         => 'IN_public_allow',
    priority      => 0,
    args          => '-m set --match-set DYNAMIC-EXTERNAL-6 dst,dst -j ACCEPT',
  }

  firewalld_direct_rule { 'Internal services ipv4':
    ensure        => present,
    inet_protocol => 'ipv4',
    table         => 'filter',
    chain         => 'IN_internal_allow',
    priority      => 0,
    args          => '-m set --match-set DYNAMIC-INTERNAL dst,dst -j ACCEPT',
  }

  firewalld_direct_rule { 'Internal services ipv6':
    ensure        => present,
    inet_protocol => 'ipv6',
    table         => 'filter',
    chain         => 'IN_internal_allow',
    priority      => 0,
    args          => '-m set --match-set DYNAMIC-INTERNAL-6 dst,dst -j ACCEPT',
  }

  firewalld_direct_rule { 'External services ipv4 fwd':
    ensure        => present,
    inet_protocol => 'ipv4',
    table         => 'filter',
    chain         => 'FWDI_public_allow',
    priority      => 0,
    args          => '-m set --match-set DYNAMIC-EXTERNAL dst,dst -j ACCEPT',
  }

  firewalld_direct_rule { 'External services ipv6 fwd':
    ensure        => present,
    inet_protocol => 'ipv6',
    table         => 'filter',
    chain         => 'FWDI_public_allow',
    priority      => 0,
    args          => '-m set --match-set DYNAMIC-EXTERNAL-6 dst,dst -j ACCEPT',
  }

  firewalld_direct_rule { 'Internal services ipv4 fwd':
    ensure        => present,
    inet_protocol => 'ipv4',
    table         => 'filter',
    chain         => 'FWDI_internal_allow',
    priority      => 0,
    args          => '-m set --match-set DYNAMIC-INTERNAL dst,dst -j ACCEPT',
  }

  firewalld_direct_rule { 'Internal services ipv6 fwd':
    ensure        => present,
    inet_protocol => 'ipv6',
    table         => 'filter',
    chain         => 'FWDI_internal_allow',
    priority      => 0,
    args          => '-m set --match-set DYNAMIC-INTERNAL-6 dst,dst -j ACCEPT',
  }

  $internal_interfaces_not_bridge.each |String $dev| {
    firewalld_direct_rule { "Allow docker0 outbound ipv4 via ${dev}":
      ensure        => present,
      inet_protocol => 'ipv4',
      table         => 'filter',
      chain         => 'FWDI_internal_allow',
      priority      => 0,
      args          => "-i docker+ -o ${dev} -j ACCEPT",
    }

    firewalld_direct_rule { "Allow docker0 outbound ipv6 via ${dev}":
      ensure        => present,
      inet_protocol => 'ipv6',
      table         => 'filter',
      chain         => 'FWDI_internal_allow',
      priority      => 0,
      args          => "-i docker+ -o ${dev} -j ACCEPT",
    }

    firewalld_direct_rule { "Allow br-+ outbound ipv4 via ${dev}":
      ensure        => present,
      inet_protocol => 'ipv4',
      table         => 'filter',
      chain         => 'FWDI_internal_allow',
      priority      => 0,
      args          => "-i br-+ -o ${dev} -j ACCEPT",
    }

    firewalld_direct_rule { "Allow br-+ outbound ipv6 via ${dev}":
      ensure        => present,
      inet_protocol => 'ipv6',
      table         => 'filter',
      chain         => 'FWDI_internal_allow',
      priority      => 0,
      args          => "-i br-+ -o ${dev} -j ACCEPT",
    }

    firewalld_direct_rule { "Allow vpn0 outbound ipv4 to internet via ${dev}":
      ensure        => present,
      inet_protocol => 'ipv4',
      table         => 'filter',
      chain         => 'FWDI_internal_allow',
      priority      => 0,
      args          => "-i vpn+ -o ${dev} -j ACCEPT",
    }

    firewalld_direct_rule { "Allow vpn0 outbound ipv6 to internet via ${dev}":
      ensure        => present,
      inet_protocol => 'ipv6',
      table         => 'filter',
      chain         => 'FWDI_internal_allow',
      priority      => 0,
      args          => "-i vpn+ -o ${dev} -j ACCEPT",
    }
  }

  firewalld_direct_rule { 'Allow traffic towards vpn0 ipv4':
    ensure        => present,
    inet_protocol => 'ipv4',
    table         => 'filter',
    chain         => 'FWDI_internal_allow',
    priority      => 0,
    args          => "-o vpn+ -j ACCEPT",
  }

  firewalld_direct_rule { 'Allow traffic towards vpn0 ipv6':
    ensure        => present,
    inet_protocol => 'ipv6',
    table         => 'filter',
    chain         => 'FWDI_internal_allow',
    priority      => 0,
    args          => "-o vpn+ -j ACCEPT",
  }
}
