Facter.add('disks_ata_wdc') do
  setcode do
    Facter::Core::Execution.execute("find /dev/disk/by-id -name 'ata-WDC*' -not -name '*part*'").lines.map(&:chomp)
  end
end
