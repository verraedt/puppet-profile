class profile::role::desktop::packages (
  Array[String] $install = [],
) {
  package { $install:
    ensure => installed,
  }
}
