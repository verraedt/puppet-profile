class profile::role::dynamic_firewall (
  String $consul_address = 'http://localhost:8500',
  String $consul_token = lookup('profile::role::server::registrator::consul_token')
) {
  package { 'dynamic-firewall':
    ensure => latest, #lint:ignore:package_ensure
    notify => Service['dynamic-firewall'],
  }

  $config = @(EOT)
    OPTIONS="--consul <%= $consul_address %>"
    CONSUL_HTTP_TOKEN="<%= $consul_token %>"
    | EOT

  file { '/etc/default/dynamic-firewall':
    ensure  => file,
    content => inline_epp($config),
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    require => Package['dynamic-firewall'],
    notify  => Service['dynamic-firewall'],
  }

  service { 'dynamic-firewall':
    ensure => running,
    enable => true,
  }
}
